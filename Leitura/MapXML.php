<?php
namespace Rubeus\IntegracaoTotvs\Leitura;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;
use Rubeus\Servicos\XML\XML;

class MapXML{
    private $dataServer;

    public function setDataServer($dataServer,$chave,$map,$tipo){
        $this->dataServer[$dataServer] = FabricaDataServer::iniciar($dataServer,$chave,$map,$tipo);
    }

    public function getDataServer($dataServer){
        return $this->dataServer[$dataServer];
    }

    public function getDataServerPorEntidade($entidade){
        $dataServerNecessario = array();
        foreach($this->dataServer as $dataServer){
            for($i=0;$i<count($entidade);$i++){
                if(isset($dataServer->getMapa()[$entidade])){
                    $dataServerNecessario[] = $dataServer;
                    break;
                }
            }
        }

    }
}
