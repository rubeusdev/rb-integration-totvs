<?php
namespace Rubeus\IntegracaoTotvs\Leitura;

abstract class FabricaDataServer{
    private static $chaveDataServer;

    public static function iniciar($dataServer, $chave,$map,$tipo){
        $chaves = explode(';',$chave);
        return  new DataServer($dataServer, $chave, self::entidade($map, $chaves,$tipo));
    }

    private static function entidade($mapa, $chaves,$tipo){
        $map = array();
        self::$chaveDataServer = array();
        $endereco = false;
        foreach ($mapa as $valor){
            switch ($tipo) {
                case 'processo': case 'processoFinanceiro':
                    $endereco = rtrim($valor['endereco']);
                    break;
                default:
                        $entidade = rtrim($valor['nome']);
                        $map[$entidade.'['.rtrim($valor['attr']).']'] = self::propriedade($valor, $chaves, $entidade);
                        $map[$entidade.'['.rtrim($valor['attr']).']']['attr'] = rtrim($valor['attr']);
                        $map[$entidade.'['.rtrim($valor['attr']).']']['deletar'] = rtrim($valor['deletar']);
                        $map[$entidade.'['.rtrim($valor['attr']).']']['naoInserir'] = rtrim($valor['naoInserir']);
                    break;
            }
        }
        $map['campoChave'] = self::$chaveDataServer;
        if ($endereco) {
            $map['enderecoArquivo'] = $endereco;
        }
        return $map;
    }

    private static function propriedade($mapa,$chaves, $entidade){
        $local = array();
        $totvs = array();
        $valor = array();
        $filtro = array();
        foreach ($mapa as $chave => $propriedade){
            if(trim($chave) == "filtro"){
                foreach($propriedade->attributes() as $key => $value){
                    if($key == 'classe_filtro'){
                        $filtro[] = ['classe' => trim($value), 'tipo' => 'classe'];
                    }else{
                        $filtro[] = ['campo' => $key, 'valor' => trim($value), 'tipo' => 1];
                    }
                }
                continue;
            }
            $local[] = array('atributo' => rtrim($propriedade['atributo']),
                           'valores' => explode(',',rtrim($propriedade['valores'])),
                           'acao' => rtrim($propriedade['acao']),
                           'acaoEnvio' => rtrim($propriedade['acaoEnvio']),
                           'notNull' => rtrim($propriedade['notNull']),
                           'uppercase' => rtrim($propriedade['uppercase']),
                           'default' => rtrim($propriedade['default']),
                           'naoEnviarPadrao' => rtrim($propriedade['naoEnviarPadrao']),
                           'naoEnviarPadraoPai' => rtrim($propriedade['naoEnviarPadraoPai']),
                           'atributoReserva' => rtrim($propriedade['atributoReserva']),
                           'naoSobrescreverDadoTotvs' => rtrim($propriedade['naoSobrescreverDadoTotvs']),
                           'id' => rtrim($propriedade['id']));

            $totvs[] = array('atributo' => rtrim($propriedade['ws']),
                           'tabela' => rtrim($propriedade['tabelaws']));

            $valor[] = self::valor($propriedade);
            if(in_array(rtrim($propriedade['ws']),$chaves)/* && !isset(self::$chaveDataServer[rtrim($propriedade['ws'])])*/){
                if(!isset(self::$chaveDataServer[rtrim($propriedade['ws'])])) self::$chaveDataServer[rtrim($propriedade['ws'])] = [];
                self::$chaveDataServer[rtrim($propriedade['ws'])][] = ['entidade' => $entidade,
                                                                    'atributo' =>  rtrim($propriedade['atributo']),
                                                                    'attr' => rtrim($mapa['attr'])];
            }
        }
        return ['local'=>$local,'totvs'=>$totvs,'valor'=>$valor,'filtro'=>$filtro];
    }

    private static function valor($mapa){
        $mapValor = array();
        foreach ($mapa->valor as $valor){
            $mapValor[] = array('atributo' => rtrim($valor['atributo']),
                                'ws' => rtrim($valor['ws']));
        }
        return $mapValor;
    }
}
