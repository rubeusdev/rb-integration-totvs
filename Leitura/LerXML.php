<?php
namespace Rubeus\IntegracaoTotvs\Leitura;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;
use Rubeus\Servicos\XML\XML;
use Rubeus\IntegracaoTotvs\Exception\ExceptionMapXML;

class LerXML{
    private $endereco;

    public function __construct(){
        $this->endereco = Conteiner::get('enderecoMapIntegracaoTotvs');
    }

    public function  ler(){
        $xml = XML::ler($this->endereco);
        if(!$xml){
            throw new ExceptionMapXML('Não foi possível ler o mapeamento!!! '.$this->endereco.(defined('URL_PROJETO') ? URL_PROJETO : ''));
        }
        return $this->iniciarObj($xml);
    }

    private function  iniciarObj($xml){
        $mapXML = new MapXML();
        foreach ($xml as $chave=>$valor){
            $mapXML->setDataServer(rtrim($chave),rtrim($valor['chave']),$valor, rtrim($valor['tipo']));
        }
        return $mapXML;
    }
}
