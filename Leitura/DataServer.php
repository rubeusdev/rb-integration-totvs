<?php 
namespace Rubeus\IntegracaoTotvs\Leitura;

class DataServer{
    private $chave;
    private $nome;
    private $map;
    
    public function __construct($nome, $chave, $map){
        $this->nome = $nome;
        $this->chave = $chave;
        $this->map = $map;
    }
    
    public function getMapa(){
        return $this->map;
    }
    
    public function getChave(){
        return $this->chave;
    }
    
    public function getCampoChave($chave){
        return $this->map['campoChave'][$chave];
    }
}