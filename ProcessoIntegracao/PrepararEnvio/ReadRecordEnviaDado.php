<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\IntegracaoTotvs\Envio\PopularObjeto;
use Rubeus\IntegracaoTotvs\Envio\BaseXML;
use Rubeus\ContenerDependencia\Conteiner;

class ReadRecordEnviaDado{
    private $popularObjeto;
    private $baseXML;

    public function __construct() {
        $this->popularObjeto = new PopularObjeto();
        $this->baseXML = new BaseXML();
    }

    public function  preparar($acao,$receberObjeto,$map){
        $dataServer = $map->getDataServer(rtrim($acao['dataServer']));
        $chave = explode(';',$dataServer->getChave());
        $identificador = array();
        $msg = Conteiner::get('Mensagem');
        for($i=0;$i<count($chave);$i++){
            $campoChave = $dataServer->getCampoChave($chave[$i]);
            for($j=0;$j<count($campoChave);$j++){
                if(trim($campoChave[$j]['attr']) == ''){
                    $acesso = $campoChave[$j]['entidade'];
                }else{
                    $acesso = $campoChave[$j]['attr'];
                }
                if ($msg->getCampo('NovoRegistroTotvs::'.rtrim($acao['dataServer']))->get('valor') == 1) {
                    $id = [0];
                } else {
                    $id = $receberObjeto->getPropriedade(explode('::',$acesso),$campoChave[$j]['atributo']);
                }
                if($id && !(is_array($id) && $id['er_ex'] == 1)) break;
            }
            if (strpos($id, '[') === 0) {
                $verificarJson = json_decode($id);
                if (!is_null($verificarJson) && is_array($verificarJson)) {
                    $id = $verificarJson[0];
                }
            }
            if(!$id || empty($id) || trim($id) == ''){
                $id = [0];
            }else if(!is_array($id)){
                $id = [$id];
            }
            $identificador[] = $id;
        }
        $array = $this->dividirInArray($identificador);

        return $array;
    }

    private function dividirInArray($identificador){
        $array = array();
        if(is_array($identificador[0])){
            for($i=0;$i<count($identificador);$i++){
                for($j=0;$j<count($identificador[$i]);$j++){
                    if(is_array($identificador[$i][$j])){
                        $array[$j][] = $identificador[$i][$j][0];
                    }else{
                         $array[$j][] = $identificador[$i][$j];
                    }
                }
            }
        }else{
            for($j=0;$j<count($identificador);$j++){
                if(is_array($identificador[$j])){
                    $array[0][] = $identificador[$j][0];
                }else{
                     $array[0][] = $identificador[$j];
                }
            }
        }
        $resultado = array();
        for($i=0;$i<count($array);$i++){
            /*for($j=0;$j<count($array[$i]);$j++){
                if(trim($array[$i][$j]) == '' || $array[$i][$j] == 0){
                    for($k=0;$k<count($array[$i]);$k++){
                        $array[$i][$k] = 0;
                    }
                    break;
                }
            }*/
            $resultado[] = implode(';', $array[$i]);
        }
        return $resultado;
    }

    public function  montarBase($objeto,$data,$dataServer,$mapa){
        $dataServerObj = $mapa->getDataServer($dataServer);

        $base = $this->baseXML->addBase($data,$dataServer);
        /*'if($base){*/
            $this->popularObjeto->popular($objeto,$base,$dataServerObj->getMapa());
            return true;
        /*}
        return false;*/
    }

    public function  getBaseXML(){
        return $this->baseXML;
    }

}
