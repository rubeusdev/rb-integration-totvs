<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\IntegracaoTotvs\Entrada\LimparBase;
use Rubeus\ContenerDependencia\Conteiner;

class Desativar{
    private $limparBase;
    
    public function __construct() {
        $this->limparBase = new LimparBase();
    }
        
    public function  preparar($acao,$receberDado,$map){
        $dataServer = $map->getDataServer(rtrim($acao['dataServer']));
        $chave = explode(';',$dataServer->getChave());
        $mensagem = Conteiner::get('Mensagem');
        $identificador = array();
        if(!is_array($chave) || trim($chave[0]) == '') return '';        
        for($i=0;$i<count($chave);$i++){  
            $campo = $mensagem->getCampo(rtrim($acao->chave[$chave[$i]]))->get('valor');
            if(trim($campo) != ''){
                $identificador[$chave[$i]] = $campo;
            }
        }
        return $identificador;
    }
    
    public function  limparBase($campo, $map,$nomeDataServer){
        if(!empty($campo)){
            $this->limparBase->popular($campo, $map, $nomeDataServer);
        }
    }
}
