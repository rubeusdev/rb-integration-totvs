<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\ContenerDependencia\Conteiner;

class ReadRecord{
    
    public function  preparar($acao,$receberDado,$map){
        $dataServer = $map->getDataServer(rtrim($acao['dataServer']));
        $chave = explode(';',$dataServer->getChave());
        $mensagem = Conteiner::get('Mensagem');
        $identificador = array();
        
        for($i=0;$i<count($chave);$i++){  
            $id = false;
            if(isset($acao->chave)){
                $id = $mensagem->getCampo(rtrim($acao->chave[$chave[$i]]))->get('valor');
                if(!$id){
                    $attr = explode('::',rtrim($acao->chave[$chave[$i]]));
                    $id = $receberDado->getPropriedade($attr[0],$attr[1],$attr[2]);
                }
            }
            if(isset($acao['idReadView']) && !$id){
                $id = $receberDado->getPropriedade(rtrim($acao['idReadView']),rtrim($acao['tabela']),$chave[$i]);
            }
            $identificador[] = $id;
        }
        return $this->dividirInArray($identificador);
    }
    
    private function dividirInArray($identificador){
        $array = array();
        if(is_array($identificador[0])){
            for($i=0;$i<count($identificador);$i++){
                for($j=0;$j<count($identificador[$i]);$j++){
                    $array[$j][] = $identificador[$i][$j];
                }
            }
        }else{
            $array[] = $identificador;
        }
        $resultado = array();
        for($i=0;$i<count($array);$i++){            
            $resultado[] = implode(';', $array[$i]);
        }
        
        return $resultado;
    }
    
    public function  atualizarBase($data,$map,$dataServer){
        $enviarBanco =  Conteiner::get('EnviarBanco');
        $enviarBanco->popular($data['data'], $map, $dataServer);
    }
    
}
