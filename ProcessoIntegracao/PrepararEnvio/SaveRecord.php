<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class SaveRecord{
    private $baseXML;

    public function __construct() {
        $this->baseXML = Conteiner::get('readRecordEnviaDado')->getBaseXML();
    }

    public function  preparar($acao=false,$receberObjeto=false,$map=false){
        return $this->baseXML->getXML(rtrim($acao['dataServer']));
    }

    public function  atualizarBase($valor,$campoEntidade, $objeto,$indice){
        $valorSetar = explode(';',$valor);

        if(!isset($valorSetar[$indice]) ||
                $valorSetar[$indice] == '0'){
            Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), $valor);
            return false;
        }
        $campo = explode('::',$campoEntidade);
        $obj = $this->acessar($objeto[$campo[0]],$campo,1);
        if(is_object($obj)){
            $obj->set($campo[count($campo)-1], $valorSetar[$indice]);
            $obj->salvar();
        }
        Persistencia::commit();
        return true;
    }

    private function acessar($objeto,$indice,$i=0){
        if(isset($indice[$i]) && is_object($objeto)){
            $obj = $objeto->get($indice[$i]);
            if(is_object($obj)){
                return $this->acessar($obj, $indice, $i+1);
            }
        }
        return $objeto;
    }
}
