<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\ContenerDependencia\Conteiner;

class GetReadView{
        
    public function  preparar($acao,$receberDado,$map=false){
        $filtro = array();
        foreach ($acao->filtro->attributes() as $chave => $valor){
            $filtro[] = rtrim($chave).$this->getValor(rtrim($valor),$receberDado);
        }
        return array('('.implode(') AND (',$filtro).')');
    }
    
    private function getValor($valor,$receberDado){
        $pOperador = explode('||',$valor);
        if(count($pOperador) == 1){
            $operador = '=';
            $pValor = $pOperador[0];
        }else{
            $operador = $pOperador[0];
            $pValor = $pOperador[1];
        }
        
        $ppValor = explode('::',$pValor);
        if(count($ppValor) == 1){
            $valorFinal = $ppValor[0];            
        }else{
            $valorFinal = $receberDado->getPropriedade($ppValor[0],$ppValor[1],$ppValor[2]);            
        }        
        if(is_array($valorFinal) && $operador == '='){
            return " in(".implode(',',$valorFinal).")";
        }
        if(trim($valorFinal) === 'now()'){
            return $operador." '".date('Y-m-d H:i:s')."'";
        }
        return $operador." ".$valorFinal."";
    }
    
    public function  atualizarBase($data,$map,$dataServer){
        $enviarBanco = Conteiner::get('EnviarBanco');
        $enviarBanco->popular($data['data'], $map, $dataServer);
    }
    
}
