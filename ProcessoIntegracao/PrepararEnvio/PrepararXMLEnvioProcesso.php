<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;

class PrepararXMLEnvioProcesso{

    public function  preparar($acao,$dados, $map,$indice=false){
        $dataServer = $map->getDataServer(rtrim($acao['dataServer']));
        $mapa = $dataServer->getMapa();

        if(isset($dados[0])) $dadosRepetir = $dados[0];
        else $dadosRepetir = $dados;
        $substituir = $procurar = array();
        foreach($dadosRepetir as $key => $value){
            $procurar[] = '{{'.$key.'}}';
            $substituir[] = $value;
        }

        if($indice === false){
            $arquivoBaseProcesso = file_get_contents(DIR_BASE.'/'.$mapa['enderecoArquivo']);
            if(strpos($arquivoBaseProcesso,'{{repeat}}') === false){
                return str_replace($procurar, $substituir, $arquivoBaseProcesso);
            }else{
                $incioRepeticao = strpos($arquivoBaseProcesso,'{{repeat}}');
                $fimRepeticao = strpos($arquivoBaseProcesso,'{{fim-repeat}}') -strpos($arquivoBaseProcesso,'{{repeat}}');

                $parteRepetir = substr($arquivoBaseProcesso, $incioRepeticao, $fimRepeticao);

                $xmlBase = str_replace($procurar, $substituir, str_replace($parteRepetir, '{{incluir-resultado-repeticao}}', $arquivoBaseProcesso));
                $resultadoRepeticao = [];
                for($i=0;$i<count($dados); $i++){
                    $substituir = $procurar = array();
                    foreach($dados[$i] as $key => $value){
                        $procurar[] = '{{'.$key.'}}';
                        $substituir[] = $value;
                        $procurar[] = '{{indiceRepeticao}}';
                        $substituir[] = $i + 10;
                    }
                    $resultadoRepeticao[] = str_replace($procurar, $substituir,$parteRepetir);
                }
                return str_replace(['{{repeat}}', '{{fim-repeat}}'],['',''],str_replace('{{incluir-resultado-repeticao}}',implode(' ',$resultadoRepeticao), $xmlBase));
            }
        }else{
            $arquivo = explode(';',$mapa['enderecoArquivo']);
            return str_replace($procurar, $substituir, file_get_contents(DIR_BASE.'/'.$arquivo[$indice]));
        }
    }

}
