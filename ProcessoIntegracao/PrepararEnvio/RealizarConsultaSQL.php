<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\PrepararEnvio;
use Rubeus\ContenerDependencia\Conteiner;

class RealizarConsultaSQL{
    private $enviarBanco;
    
    public function __construct() {
        $this->enviarBanco =  Conteiner::get('EnviarBanco');
    }
    
    
    public function  preparar($acao,$receberDado,$map){
        $dataServer = $map->getDataServer(rtrim($acao['dataServer']));
        $chave = explode(';',$dataServer->getChave());
        $mensagem = Conteiner::get('Mensagem');
        $identificador = array();
        
        if(!is_array($chave) || trim($chave[0]) == ''){
            if(isset($acao->limite)){
                $mensagem->setCampo('Fim',$mensagem->getCampo('Inicio')->get('valor',0)+trim($acao->limite['QTD']));

                $limite = 'INICIO='.$mensagem->getCampo('Inicio')->get('valor',0).
                                ';FIM='.$mensagem->getCampo('Fim')->get('valor',trim($acao->limite['QTD']));

                $mensagem->setCampo('Inicio',$mensagem->getCampo('Fim')->get('valor'));
                return $limite;
            } 
            return '';
        }
        for($i=0;$i<count($chave);$i++){  
            $id = false;
            if(isset($acao->chave)){
                $id = $mensagem->getCampo(rtrim($acao->chave[$chave[$i]]))->get('valor');
                if(!$id){
                    $attr = explode('::',rtrim($acao->chave[$chave[$i]]));
                    $id = $receberDado->getPropriedade($attr[0],$attr[1],$attr[2]);
                }
            }
            if(isset($acao['idReadView']) && !$id){
                $id = $receberDado->getPropriedade(rtrim($acao['idReadView']),rtrim($acao['tabela']),$chave[$i]);
            }
            $identificador[] = $chave[$i].'='.$id;
        }
        if(isset($acao->limite)){
            $mensagem->setCampo('Fim',$mensagem->getCampo('Inicio')->get('valor',0)+trim($acao->limite['QTD']));
            
            $limite = ';INICIO='.$mensagem->getCampo('Inicio')->get('valor',0).
                            ';FIM='.$mensagem->getCampo('Fim')->get('valor',trim($acao->limite['QTD']));
            
            $mensagem->setCampo('Inicio',$mensagem->getCampo('Fim')->get('valor'));
            return $this->dividirInArray($identificador).$limite;
        }
        return $this->dividirInArray($identificador);
    }
    
    private function dividirInArray($identificador){
        $array = array();
        if(is_array($identificador[0])){
            for($i=0;$i<count($identificador);$i++){
                for($j=0;$j<count($identificador[$i]);$j++){
                    $array[$j][] = $identificador[$i][$j];
                }
            }
        }else{
            $array = $identificador;
        }
        $resultado = implode(';', $array);
        return $resultado;
    }
    
    public function  atualizarBase($data,$map,$dataServer){
        $this->enviarBanco->popular($data['data'], $map, $dataServer);
    }
     
}
