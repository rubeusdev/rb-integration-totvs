<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao;
use Rubeus\IntegracaoTotvs\Registrar\ErroChamadaTotvs;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ErroProcessoIntegracao{
    private $erroProcesso;

    public function  registrar($registroChamada,$erro){
        $erroChamadaTotvs = new ErroChamadaTotvs();
        $this->identificarTipo($erro,$erroChamadaTotvs);
        $erroChamadaTotvs->setRegistroChamadaTotvs($registroChamada);
        $erroChamadaTotvs->setMomento(date('Y-m-d H:i:s'));
        $erroChamadaTotvs->setAtivo(1);
        $this->erroProcesso[] = $erroChamadaTotvs;
    }

    public function  getErro(){
        $erroMaisRelevante = [];
        for($i=0;$i<count($this->erroProcesso);$i++){
            if(!isset($erroMaisRelevante['relevancia']) ||
                    $erroMaisRelevante['relevancia'] < $this->erroProcesso[$i]->getRelevancia()){

                $erroMaisRelevante = ['relevancia' => $this->erroProcesso[$i]->getRelevancia(),
                                    'texto' => $this->erroProcesso[$i]->getTextoErroGeral()];
            }
        }
        if(count($erroMaisRelevante) > 0){
            Conteiner::registrar('registroMensagemErroIntegracaoTotvs',$erroMaisRelevante['texto']);
        }
        $this->registrarErro();
        return $erroMaisRelevante;
    }

    public function  atualizarProcesso($id){
        for($i=0;$i<count($this->erroProcesso);$i++){
            $this->erroProcesso[$i]->setRegistroChamadaTotvs($id);
        }
    }

    public function  registrarErro(){
        for($i=0;$i<count($this->erroProcesso);$i++){
            $this->erroProcesso[$i]->salvar();
        }
        Persistencia::commit();
        $this->erroProcesso = [];
    }

    private function cortarErro($erro){
        $texto = explode('========',$erro);
        return str_replace('&#xD;',' ',$texto[0]);
    }

    private function identificarTipo($erro,$erroChamadaTotvs){
        $registroErro = [];
        $erroChamadaTotvs->setTextoErro($this->cortarErro($erro));
        switch (true) {
            case strpos($erroChamadaTotvs->getTextoErro(),'permite somente 1 inscrição(ões)'):
                $erroChamadaTotvs->setTextoErroGeral('Você já possui inscrição neste processo seletivo.');
                $erroChamadaTotvs->setTipoErro(2);
                $erroChamadaTotvs->setRelevancia(20);
                break;
            case trim($erroChamadaTotvs->getTextoErro()) == '':
                $erroChamadaTotvs->setTextoErroGeral('Não foi possível se comunicar com o servidor.');
                $erroChamadaTotvs->setTipoErro(3);
                $erroChamadaTotvs->setRelevancia(10);
                break;
            default:
                $erroChamadaTotvs->setTextoErroGeral('Ocorreu um erro tente novamente.');
                $erroChamadaTotvs->setTipoErro(1);
                $erroChamadaTotvs->setRelevancia(0);
                break;
        }
        return $registroErro;
    }


}
