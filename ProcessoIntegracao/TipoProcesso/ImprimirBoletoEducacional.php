<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ImprimirBoletoEducacional{
    private $dadosProcesso;

    public function  executar($processo,$contexto){
        
        $this->dadosProcesso = [];
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $msg = Conteiner::get('Mensagem');
        $processoIntegracao = Conteiner::get('ProcessoIntegracao');
        $map = Conteiner::get('MapTotvs');

        foreach ($processo->acao as $acao) {
            $resultado = $clienteTotvsWS->imprimirBoletoEducacional(
                $msg->getCampo('idBoleto')->get('valor'),
                $msg->getCampo('codColigada')->get('valor'),
                $msg->getCampo('codFilial')->get('valor'),
                $msg->getCampo('tipoCurso')->get('valor')
            );
                            
            $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', ':s'],'',$resultado);
            $conteudo = str_replace(['body'], "Body", str_replace(['</s:','</soap:'],"</",str_replace(['<s:','<soap:'],"<",$xmlValido)));

            $resposta = simplexml_load_string($conteudo);
            
            $respostaTotvs = $resposta->Body->ImprimeBoletoParamResponse->ImprimeBoletoParamResult;
            if ($respostaTotvs) {
                return $respostaTotvs;
            } else {
                Conteiner::get('ErroProcessoIntegracao')->registrar(
                    Conteiner::get('ultimoRegistroChamadaTotvs'),
                    $resposta->Body->ImprimeBoletoParamResponse->ImprimeBoletoParamResult
                );
            }
        }
        return false;
    }

}
