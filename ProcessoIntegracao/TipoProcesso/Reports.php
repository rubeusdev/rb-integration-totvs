<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class Reports{
    private $dadosProcesso;


    public function  executar($processo,$contexto){
        $this->dadosProcesso = [];
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $disponiblilidade = true;
        $msg = Conteiner::get('Mensagem');
        $processoIntegracao = Conteiner::get('ProcessoIntegracao');
        $map = Conteiner::get('MapTotvs');

        foreach($processo->acao as $acao){
            switch (rtrim($acao['metodo'])) {
                case 'processo':
                    $this->dadosProcesso[rtrim($acao['id'])] = $processoIntegracao->iniciar(rtrim($acao['dataServer']));
                    break;
                case 'processoMsg':
                    $this->dadosProcesso[rtrim($acao['id'])] = $msg->getCampo(rtrim($acao['dataServer']))->get('valor');
                    break;
                default:
                    if($this->dadosProcesso[rtrim($acao['idDados'])]){
                        $metodo = Conteiner::get(rtrim($acao['metodo']));
                        $filtro = $metodo->preparar($acao,$this->dadosProcesso[rtrim($acao['idDados'])][0], $map,0);
                        $parametro = $metodo->preparar($acao,$this->dadosProcesso[rtrim($acao['idDados'])][0], $map,1);

                        $codRelatorio = rtrim($acao['codRelatorio']);
                        $codColigada = rtrim($acao['codColigada']);

                        if($this->dadosProcesso[rtrim($acao['idDados'])][0][$codRelatorio]){
                            $codRelatorio = $this->dadosProcesso[rtrim($acao['idDados'])][0][$codRelatorio];
                            $codColigada = $this->dadosProcesso[rtrim($acao['idDados'])][0][$codColigada];
                        }else if($msg->getCampo($codRelatorio)->get('valor')){
                            $codRelatorio = $msg->getCampo($codRelatorio)->get('valor');
                            $codColigada = $msg->getCampo($codColigada)->get('valor');
                        }

                        $resultado = $clienteTotvsWS->report($codColigada,$codRelatorio,$filtro,$parametro);

                        if(trim($resultado) == ''  || strpos($resultado, 'DOCTYPE HTML')){
                            $disponiblilidade = false;
                            Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), '');
                        }
                    }
                    break;
            }

        }
        if($disponiblilidade){
            $resultado = simplexml_load_string( str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', 's:Body', 's:Envelope'],
                                            ['','', '', '', 'sBody', 'sEnvelope'],$resultado));
            return $resultado->sBody->GetFileChunkResponse->GetFileChunkResult;
        }
        return $disponiblilidade;
    }


}
