<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\IntegracaoTotvs\Entrada\RetornoConsulta;
use Rubeus\Bd\Persistencia;

class ProcessoConsultaRBConsulta{
    private $consultar;

    public function __construct() {
        $this->consultar = new RetornoConsulta();
    }

    public function  executar($processo,$contexto){
        $receberData = Conteiner::get('ReceberDataTotvs');
        $map = Conteiner::get('MapTotvs');

        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $resultado = false;
        $disponiblilidade = true;
        foreach($processo->acao as $acao){
            $metodo = Conteiner::get(rtrim($acao['metodo']));
            $mensagem->setCampo('Inicio',0);

            do{
                $campo = $metodo->preparar($acao,$receberData,$map);

                if(rtrim($acao['metodo']) == 'desativar'){
                    $metodo->limparBase($campo,$map, rtrim($acao['dataServer']));
                }else{
                    Persistencia::commit();

                    $resultado = $clienteTotvsWS->{rtrim($acao['metodo'])}(rtrim($acao['dataServer']),
                                                                                rtrim($acao->filtro['codColigada']),
                                                                                rtrim($acao->filtro['codSistema']),
                                                                                $campo);

                    $i++;

                    if(trim($resultado) == '' || strpos($resultado, 'DOCTYPE HTML') !== false){
                        $disponiblilidade = false;
                        Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), '');
                        break;
                    }

                    $objeto = $receberData->lerData($resultado,rtrim($acao['id']),(rtrim($acao['registrar']) == 1), true,'realizarConsultaSQL');
                    $qtdRegistro = count($objeto['data']->Resultado);

                    //if(rtrim($acao['bd']) == 1){
                         $resultado = $this->consultar->montarRetorno($objeto['data']);
                    //}
                    Persistencia::commit();
                }
            }while(isset($acao->limite) && trim($acao->limite['QTD']) > 0 && $qtdRegistro >= trim($acao->limite['QTD']));

            if(!$disponiblilidade){
                break;
            }
        }
        return $resultado;
    }

}
