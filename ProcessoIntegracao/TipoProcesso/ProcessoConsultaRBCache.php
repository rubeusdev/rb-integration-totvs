<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ProcessoConsultaRBCache{

    public function  executar($processo,$contexto){
        $receberData = Conteiner::get('ReceberDataTotvs');
        $map = Conteiner::get('MapTotvs');

        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $disponiblilidade = true;
        foreach($processo->acao as $acao){
            $metodo = Conteiner::get(rtrim($acao['metodo']));
            $mensagem->setCampo('Inicio',0);

            do{
                $campo = $metodo->preparar($acao,$receberData,$map);

                if(rtrim($acao['metodo']) == 'desativar'){
                    $metodo->limparBase($campo,$map, rtrim($acao['dataServer']));
                }else{
                    Persistencia::commit();
                    $msg = Conteiner::get('Mensagem');

                    $resultado = $clienteTotvsWS->arquivoCache($msg->getCampo("idReal")->get('valor'));

                    $i++;
                    if(trim($resultado) == ''){
                        $disponiblilidade = false;
                        break;
                    }

                    $objeto = $receberData->lerData($resultado,rtrim($acao['id']),(rtrim($acao['registrar']) == 1));
                    $qtdRegistro = count($objeto['data']->Resultado);

                    if(rtrim($acao['bd']) == 1){
                        $metodo->atualizarBase($objeto,$map,rtrim($acao['dataServer']));
                    }
                    Persistencia::commit();
                }
            }while(isset($acao->limite) && trim($acao->limite['QTD']) > 0 && $qtdRegistro >= trim($acao->limite['QTD']));

            if(!$disponiblilidade){
                break;
            }
        }
        return $disponiblilidade;
    }

}
