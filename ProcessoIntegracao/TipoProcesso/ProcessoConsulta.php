<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ProcessoConsulta{

    public function  executar($processo,$contexto){
        $receberData = Conteiner::get('ReceberDataTotvs');

        $map = Conteiner::get('MapTotvs');

        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $disponiblilidade = true;
        foreach($processo->acao as $acao){
            $metodo = Conteiner::get(rtrim($acao['metodo']));
            $campo = $metodo->preparar($acao,$receberData,$map);

            if(rtrim($acao['metodo']) == 'desativar'){
                $metodo->limparBase($campo,$map, rtrim($acao['dataServer']));
            }else{
                for($i=0;$i<count($campo);$i++){
                    Persistencia::commit();
                    $resultado = $clienteTotvsWS->{rtrim($acao['metodo'])}(rtrim($acao['dataServer']),
                                                                             $campo[$i],
                                                                             $contexto);

                    if(trim($resultado) == ''){
                        $disponiblilidade = false;
                        break;
                    }

                    $objeto = $receberData->lerData($resultado,rtrim($acao['id']),(rtrim($acao['registrar']) == 1), true,rtrim($acao['metodo']));

                    if(rtrim($acao['bd']) == 1){
                        $metodo->atualizarBase($objeto,$map,rtrim($acao['dataServer']));
                    }
                    Persistencia::commit();
                }
            }

            if(!$disponiblilidade){
                break;
            }
        }
        return $disponiblilidade;
    }

}
