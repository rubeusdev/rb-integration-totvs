<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ExecutarProcesso{
    private $dadosProcesso;

    public function  executar($processo,$contexto){
        $this->dadosProcesso = [];
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $disponiblilidade = true;
        $msg = Conteiner::get('Mensagem');
        $processoIntegracao = Conteiner::get('ProcessoIntegracao');
        $map = Conteiner::get('MapTotvs');

        if (json_encode($processo->acao) == $_SESSION['processo_integracao_totvs']) {
            Conteiner::get('ErroProcessoIntegracao')->registrar('Algo inesperado ocorreu', '');
            return false;
        } else {
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_start();
            }
            $_SESSION['processo_integracao_totvs'] = json_encode($processo->acao);
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_write_close();
            }
        }

        foreach($processo->acao as $acao){
            switch (rtrim($acao['metodo'])) {
                case 'processo':
                    $this->dadosProcesso[rtrim($acao['id'])] = $processoIntegracao->iniciar(rtrim($acao['dataServer']));
                    break;
                case 'processoMsg':
                    $this->dadosProcesso[rtrim($acao['id'])] = $msg->getCampo(rtrim($acao['dataServer']))->get('valor');
                    break;
                default:
                    if ($this->dadosProcesso[rtrim($acao['idDados'])]) {
                        $metodo = Conteiner::get(rtrim($acao['metodo']));
                        $xml = $metodo->preparar($acao, $this->dadosProcesso[rtrim($acao['idDados'])], $map);

                        if (rtrim($acao['comParamentro']) == 1) {
                            $resultado = $clienteTotvsWS->executarProcessoParametros(
                                rtrim($acao['dataServer']),
                                $xml,
                                (isset($acao['ultilizarLinkDataServerPadrao']) && rtrim($acao['ultilizarLinkDataServerPadrao'] == 1))
                            );
                        } else {
                            $resultado = $clienteTotvsWS->executarProcesso(
                                rtrim($acao['dataServer']),
                                $xml,
                                $msg->getCampo('usuarioTotvsAlternativo')->get('valor'),
                                $msg->getCampo('senhaTotvsAlternativo')->get('valor'),
                                (isset($acao['ultilizarLinkDataServerPadrao']) && rtrim($acao['ultilizarLinkDataServerPadrao'] == 1))
                            );
                        }

                        
                        $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', ':s'],'',$resultado);
                        $conteudo = str_replace(['body'],"Body", str_replace(['</s:','</soap:'],"</",str_replace(['<s:','<soap:'],"<",$xmlValido)));

                        $resposta = simplexml_load_string($conteudo);

                        if (rtrim($acao['comParamentro']) == 1) {
                            if ($resposta->Body->ExecuteWithParamsResponse->ExecuteWithParamsResult != 1) {
                                $disponiblilidade = false;
                                Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), $resposta->Body->ExecuteWithParamsResponse->ExecuteWithParamsResult);
                            }
                        } else {
                            if ($resposta->Body->ExecuteWithXmlParamsResponse->ExecuteWithXmlParamsResult != 1) {
                                $disponiblilidade = false;
                                Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), $resposta->Body->ExecuteWithXmlParamsResponse->ExecuteWithXmlParamsResult);
                            }
                        }
                    } else {
                        Conteiner::get('ErroProcessoIntegracao')->registrar('Processo não executado', '');
                    }
                    break;
            }
        }
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_start();
        }
        $_SESSION['processo_integracao_totvs'] = '';
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_write_close();
        }
        return $disponiblilidade;
    }

}
