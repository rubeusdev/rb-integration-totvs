<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class CriarAtendimento{
    private $dadosProcesso;

    public function  executar($processo,$contexto){
        
        $this->dadosProcesso = [];
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $disponiblilidade = true;
        $msg = Conteiner::get('Mensagem');
        $processoIntegracao = Conteiner::get('ProcessoIntegracao');
        $map = Conteiner::get('MapTotvs');

        if (json_encode($processo->acao) == $_SESSION['criar_atendimento_integracao_totvs']){
            Conteiner::get('ErroProcessoIntegracao')->registrar('Algo inesperado ocorreu', '');
            return false;
        } else {
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_start();
            }
            $_SESSION['criar_atendimento_integracao_totvs'] = json_encode($processo->acao);
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_write_close();
            }
        }

        foreach ($processo->acao as $acao) {
            $paramentros = $msg->getCampo(rtrim($acao['idDados']))->get('valor');
                    
            if ($paramentros) {
                $resultado = $clienteTotvsWS->criarAtendimento(
                    $paramentros,
                    $msg->getCampo('usuarioTotvsAlternativo')->get('valor'),
                    $msg->getCampo('senhaTotvsAlternativo')->get('valor')
                );
                                
                $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', ':s'],'',$resultado);
                $conteudo = str_replace(['body'],"Body", str_replace(['</s:','</soap:'],"</",str_replace(['<s:','<soap:'],"<",$xmlValido)));

                $resposta = simplexml_load_string($conteudo);
                $respostaTotvs = explode(';', $resposta->Body->criarAtendimentoResponse->criarAtendimentoResult);
                if (count($respostaTotvs) == 3 && $respostaTotvs[2] == (int)$respostaTotvs[2]) {
                    $msg->setCampo('atendimentoCriado', $respostaTotvs);
                } else {
                    Conteiner::get('ErroProcessoIntegracao')->registrar(
                        Conteiner::get('ultimoRegistroChamadaTotvs'),
                        $resposta->Body->criarAtendimentoResponse->criarAtendimentoResult
                    );
                }
            } else {
                Conteiner::get('ErroProcessoIntegracao')->registrar('Os paramentros não foram passados', '');
            }
        }
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_start();
        }
        $_SESSION['criar_atendimento_integracao_totvs'] = '';
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_write_close();
        }
        return $disponiblilidade;
    }

}
