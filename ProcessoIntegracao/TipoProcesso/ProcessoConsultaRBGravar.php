<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\IntegracaoTotvs\Registrar\RegistrarChamadaTotvs;

class ProcessoConsultaRBGravar{
    private $registrar;
    
    public function __construct() {
        $this->registrar = new RegistrarChamadaTotvs();
    }
    
    public function  executar($processo,$contexto){
        $receberData = Conteiner::get('ReceberDataTotvs');
        $map = Conteiner::get('MapTotvs');
        $clienteTotvsWS = Conteiner::get('ClienteLocalTotvsWS');
        $disponiblilidade = true;
        foreach($processo->acao as $acao){            
            $metodo = Conteiner::get(rtrim($acao['metodo']));
            $campo = $metodo->preparar($acao,$receberData,$map);
            
            $clienteTotvsWS->realizarConsultaSQL(rtrim($acao['dataServer']), 
                                                                rtrim($acao->filtro['codColigada']), 
                                                                rtrim($acao->filtro['codSistema']),
                                                                $campo);                        
        }        
        $this->registrarResultado(true, false,true);
        return $disponiblilidade;
    }
    
    private function registrarResultado($resultado, $endereco,$disponiblilidade){
        $msg = Conteiner::get('Mensagem');
        $msg->setCampo('AgendaProcesso::resultado',$resultado);
        $msg->setCampo('AgendaProcesso::enderecoResultado',$endereco);
        $msg->setCampo('AgendaProcesso::disponibilidade',$disponiblilidade);
    }
    
}