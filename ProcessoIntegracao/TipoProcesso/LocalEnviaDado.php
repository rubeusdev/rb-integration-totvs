<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class LocalEnviaDado{

    public function  executar($processo,$contexto){
        $registrar = Conteiner::get('RegistrarObjetos');
        $receberData = Conteiner::get('ReceberDataTotvs');
        $receberObjeto = Conteiner::get($registrar);
        $endereco = '';

        $map = Conteiner::get('MapTotvs');

        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }

        Conteiner::get('readRecordEnviaDado')->getBaseXML()->limpar();
        $msg = Conteiner::get('Mensagem');

        if (json_encode($processo->acao) == $_SESSION['data_server_integracao_totvs']){
            Conteiner::get('ErroProcessoIntegracao')->registrar('Algo inesperado ocorreu', '');
            return false;
        } else {
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_start();
            }
            $_SESSION['data_server_integracao_totvs'] = json_encode($processo->acao);
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_write_close();
            }
        }

        $resultado = $disponiblilidade = true;
        foreach($processo->acao as $acao){
            $metodo = Conteiner::get(rtrim($acao['metodo']).'EnviaDado');
            $campo = $metodo->preparar($acao,$receberObjeto,$map);
            for($i=0;$i<count($campo);$i++){
                Persistencia::commit();
                $retornoTotvs = $clienteTotvsWS->{rtrim($acao['metodo'])}(
                    rtrim($acao['dataServer']),
                    $campo[$i],
                    $contexto,
                    $msg->getCampo('usuarioTotvsAlternativo')->get('valor'),
                    $msg->getCampo('senhaTotvsAlternativo')->get('valor'),
                    (isset($acao['ultilizarLinkDataServerPadrao']) && rtrim($acao['ultilizarLinkDataServerPadrao'] == 1))
                );

                if (trim($retornoTotvs) == '' || (rtrim($acao['metodo']) == 'saveRecord' && (strpos($retornoTotvs, '===================')  || strpos($retornoTotvs, 'String')))){
                    Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), '');
                    $disponiblilidade = $resultado = false;
                    break;
                }


                if(rtrim($acao['metodo']) == 'readRecord'){
                    $resultado = $metodo->montarBase($receberObjeto, $retornoTotvs,rtrim($acao['dataServer']),$map);

                }else{
                    $valor = $receberData->lerData($retornoTotvs,false,true,false,rtrim($acao['metodo']));
                    $resultado = $metodo->atualizarBase($valor,rtrim($acao['campo']),$receberObjeto->get(),rtrim($acao['indice']));

                }
                if(!$resultado){
                    break;
                }
            }
            if(!$resultado){
                break;
            }
        }
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_start();
        }
        $_SESSION['data_server_integracao_totvs'] = '';
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_write_close();
        }
        return $resultado;
    }


}
