<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\IntegracaoTotvs\Registrar\MonitoramentoTotvs;
use Rubeus\Bd\Persistencia;

class Disponibilidade{

    public function  executar($processo,$contexto){
       
        if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        }
        
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }

        $resultado = $clienteTotvsWS->disponibilidade();
        $monitoramentoTotvs = new MonitoramentoTotvs();
        $monitoramentoTotvs->setStatusChamadaIntegracaoTotvs((trim($resultado) == '')?4:trim($resultado));
        $monitoramentoTotvs->setAtivo(1);
        $monitoramentoTotvs->setMomento(date('Y-m-d H:i:s'));
        $monitoramentoTotvs->salvar();
        Persistencia::commit();

        if (Persistencia::getBaseAtual() != "principal") {
            Persistencia::mudarBase("principal");
        }
        
        return true;
    }
}
