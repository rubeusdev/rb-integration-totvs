<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class ExecutarProcessoFinanceiro{
    private $dadosProcesso;

    public function  executar($processo,$contexto){
        $this->dadosProcesso = [];
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $clienteTotvsWS = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $clienteTotvsWS = Conteiner::get('ClienteTotvsWS');
        }
        $mensagem = Conteiner::get('Mensagem');
        $i=0;
        $disponiblilidade = true;
        $msg = Conteiner::get('Mensagem');
        $processoIntegracao = Conteiner::get('ProcessoIntegracao');
        $map = Conteiner::get('MapTotvs');
         
        if (json_encode($processo->acao) == $_SESSION['processo_financeiro_integracao_totvs']){
            Conteiner::get('ErroProcessoIntegracao')->registrar('Algo inesperado ocorreu', '');
            return false;
        } else {
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_start();
            }
            $_SESSION['processo_financeiro_integracao_totvs'] = json_encode($processo->acao);
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_write_close();
            }
        }

        foreach($processo->acao as $acao){
            if(Conteiner::get(rtrim($acao['idDados']))){
                $metodo = Conteiner::get(rtrim($acao['metodo']));
                $xml = $metodo->preparar($acao,Conteiner::get(rtrim($acao['idDados'])), $map);

                $resultado = $clienteTotvsWS->executarProcessoFinanceiro($contexto,$xml,rtrim($acao['dataServer']));

                if(trim($resultado) == ''  || strpos($resultado, 'DOCTYPE HTML')){
                    $disponiblilidade = false;
                    Conteiner::get('ErroProcessoIntegracao')->registrar(Conteiner::get('ultimoRegistroChamadaTotvs'), '');
                } else {
                    $resultadoParse = simplexml_load_string($resultado);
                    Conteiner::registrar('resultado-processo-fincanceiro-totvs', $resultadoParse->Body);
                }
            }
        }
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_start();
        }
        $_SESSION['processo_financeiro_integracao_totvs'] = '';
        if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
            session_write_close();
        }
        return $disponiblilidade;
    }

}
