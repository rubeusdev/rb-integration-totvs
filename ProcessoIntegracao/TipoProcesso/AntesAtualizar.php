<?php 
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao\TipoProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\Bd\Persistencia;

class AntesAtualizar{
    
    public function  executar($processo,$contexto){
        $mensagem = Conteiner::get('Mensagem');
        
        if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        }
        
        $registrarChamada = ConteinerEntidade::getInstancia('RegistrarChamadaWebServiceTotvs');
        
        $registrarChamada->setProcessoTotvsIdentificador(PROJETOREGISTROATIVIDADE);
        
        $registrarChamada->setProcesso($mensagem->getCampo('Processo')->get('valor'));
        $registrarChamada->setEtapa($mensagem->getCampo('Etapa')->get('valor'));
        $registrarChamada->setProcessoTotvs($mensagem->getCampo('ProcessoTotvs::id')->get('valor'));
        $registrarChamada->setProcessoTotvsChamada($mensagem->getCampo('ProcessoTotvs')->get('valor'));
        $registrarChamada->setUsuario($mensagem->getCampo('Usuario')->get('valor'));
        $registrarChamada->setFormulaVisual($mensagem->getCampo('FormulaVisual')->get('valor'));
        $registrarChamada->setCodSistema($mensagem->getCampo('CodigoSistema')->get('valor'));
        $registrarChamada->setMomentoChamada($mensagem->getCampo('Momento')->get('valor'));
        $registrarChamada->setCodigoAcao($mensagem->getCampo('CodigoAcao')->get('valor'));
        $registrarChamada->setParametro1($mensagem->getCampo('Parametro1')->get('valor'));
        $registrarChamada->setParametro2($mensagem->getCampo('Parametro2')->get('valor'));
        $registrarChamada->setParametro3($mensagem->getCampo('Parametro3')->get('valor'));
        $registrarChamada->setParametro4($mensagem->getCampo('Parametro4')->get('valor'));
        $registrarChamada->setParametro5($mensagem->getCampo('Parametro5')->get('valor'));
        $registrarChamada->setParametro6($mensagem->getCampo('Parametro6')->get('valor'));
        $registrarChamada->setParametro7($mensagem->getCampo('Parametro7')->get('valor'));
        $registrarChamada->setParametro8($mensagem->getCampo('Parametro8')->get('valor'));
        $registrarChamada->setParametro9($mensagem->getCampo('Parametro9')->get('valor'));
        $registrarChamada->setParametro10($mensagem->getCampo('Parametro10')->get('valor'));
        $registrarChamada->setAntesAtualizar($mensagem->getCampo('AntesAtualizar')->get('valor'));
        
        $registrarChamada->setMomento(date('Y-m-d H:i:s'));
        $registrarChamada->setAtivo(1);
        $registrarChamada->salvar();
        
        if (Persistencia::getBaseAtual() != "principal") {
            Persistencia::mudarBase("principal");
        }
        
        return true;
    }
}