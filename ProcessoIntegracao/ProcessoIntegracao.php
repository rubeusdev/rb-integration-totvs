<?php
namespace Rubeus\IntegracaoTotvs\ProcessoIntegracao;
use Rubeus\IntegracaoTotvs\Leitura\LerXML;
use Rubeus\IntegracaoTotvs\Registrar\ConsultarPrimeiraChamada;
use Rubeus\IntegracaoTotvs\Registrar\RegistroProcesso;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\XML\XML;

class ProcessoIntegracao{
    private $processo;
    private $map;
    private $contexto;

    public function __construct() {
        $this->processo = XML::ler(Conteiner::get('PocessoIntegracaoTotvs'));
        $lerXML = new LerXML();
        $this->map = $lerXML->ler();
        $this->contexto = $this->processo->contexto;
        Conteiner::registrar('MapTotvs',$this->map);
        Conteiner::registrar('AcaoDefinidaProcesso',$this);
    }

    public function  iniciar($processo){
        Conteiner::registrar('registroMensagemErroIntegracaoTotvs',false);
        $this->identificarProcesso($processo);
        $this->popularMensagem();
        $executar = Conteiner::get(rtrim($this->processo->{$processo}['tipo']));
        if($executar){
            $resultado = $executar->executar($this->processo->{$processo},  Conteiner::get('contextoTotvs') ? Conteiner::get('contextoTotvs') : rtrim($this->contexto));
            if(count(Conteiner::get('ErroProcessoIntegracao')->getErro()) > 0){
                Conteiner::get('ErroProcessoIntegracao')->registrarErro();
                return false;
            }
            return $resultado;
        }
        return true;
    }

    private function identificarProcesso($processo){
        $msg = Conteiner::get('Mensagem');
        $registroProcesso = new RegistroProcesso();
        $msg->setCampo('ProcessoTotvs::id', $registroProcesso->registrar($processo));
    }

    private function popularMensagem(){
        $msg = Conteiner::get('Mensagem');
        if($msg->getCampo('AntesAtualizar')->get('valor') == 0
                && (!$msg->getCampo('Parametro1')->get('valor') ||
                    is_null($msg->getCampo('Parametro1')->get('valor')))&& $msg->getCampo('ProcessoTotvs')->get('valor')){


            $consultarPrimeiraChamada = new ConsultarPrimeiraChamada();
            $dados = $consultarPrimeiraChamada->consultar($msg->getCampo('Usuario')->get('valor'),
                                                $msg->getCampo('CodigoSistema')->get('valor'),
                                                $msg->getCampo('CodigoAcao')->get('valor'));

            $msg->setCampo('Parametro1', $dados['parametro1'],'processo')->setCampo('Parametro2', $dados['parametro2'],'processo')
                    ->setCampo('Parametro3', $dados['parametro3'],'processo')->setCampo('Parametro4', $dados['parametro4'],'processo')
                    ->setCampo('Parametro5', $dados['parametro5'],'processo')->setCampo('Parametro6', $dados['parametro6'],'processo')
                    ->setCampo('Parametro7', $dados['parametro7'],'processo')->setCampo('Parametro8', $dados['parametro8'],'processo')
                    ->setCampo('Parametro9', $dados['parametro9'],'processo')->setCampo('Parametro10', $dados['parametro10'],'processo');

            $consultarPrimeiraChamada->desativar($dados['id']);
        }

    }

    public function desativarAcao($msg,$processo){
        return ($msg->getCampo('AgendaProcesso::disponibilidade')->get('valor') == 1 && $processo['recursivo'] == 0);
    }

    public function acaoFinal($msg,$idAgendaProcesso,$resultado){
        /*if(intval($resultado['success']) == 0 && $msg->getCampo('AgendaProcesso::disponibilidade')->get('valor') == 1){
            $repositorioProcesso = Conteiner::get('RepositorioProcesso');
            $msg->setCampo('EnvioEmailInconsistencia::arquivo', $msg->getCampo('AgendaProcesso::enderecoResultado')->get('valor'),'requisicao');
            $msg->setCampo('EnvioEmailInconsistencia::candidato', $msg->getCampo('Candidato::id')->get('valor'),'requisicao');
            $msg->setCampo('EnvioEmailInconsistencia::agendaProcesso', $idAgendaProcesso,'requisicao');

            $processoRepositorio = $repositorioProcesso::get('Email','enviarEmailInconsistencia');
            $processoRepositorio->executar($msg);
        }*/
    }
}
