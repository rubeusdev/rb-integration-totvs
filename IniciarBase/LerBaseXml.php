<?php 
namespace Rubeus\IntegracaoTotvs\IniciarBase;
use Rubeus\Bd\Persistencia;
use Rubeus\IntegracaoTotvs\Leitura\LerXML;
use Rubeus\IntegracaoTotvs\Entrada\ReceberData;

class LerBaseXml{
    private $enviarBanco;
    private $map;
    private $receberData;
    
    public function __construct(){
        $lerXML = new LerXML();
        $this->map = $lerXML->ler();
        $this->receberData = new ReceberData();
        $this->enviarBanco = new EnviarBanco();
        $this->estruturar = new Estruturar();
    }
     
    
    public function  executar(){
        $pasta = DIR_BASE.'/file/iniciarBase/xml';
        $diretorio = new \DirectoryIterator($pasta);
        $subPasta = [];
        foreach ($diretorio as $dir){
            if (!$dir->isDot()){
                $subPasta[] = $pasta.'/'.$dir->getFilename();
            }
        }
        sort($subPasta);
        for($i=0;$i<count($subPasta);$i++){
            $subDir = new \DirectoryIterator($subPasta[$i]);
            foreach ($subDir as $file){
                  if(!$file->isDot()){
                        $objeto = $this->receberData->lerData(file_get_contents($subPasta[$i].'/'.$file->getFilename()),
                                                                    false, true, true, 'realizarConsultaSQL');
                        $dataServer = explode('_',$file->getFilename());
                        $dataServer = explode('.', $dataServer[0]);
                        var_dump($dataServer[0],'=========');
                        $this->estruturar->popular($objeto['data'],$this->map,$dataServer[0]);
                        $this->enviarBanco->salvarBanco($this->estruturar->getObjeto(), $this->estruturar->getIdentificador()
                                , $this->estruturar->getNaoNull());
                        Persistencia::commit();
                    }
              }
        }
    }
    
}