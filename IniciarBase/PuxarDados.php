<?php
namespace Rubeus\IntegracaoTotvs\IniciarBase;
use Rubeus\ContenerDependencia\Conteiner;

class PuxarDados{
    private $puxar;
    private $clienteWebService;

    public function __construct(){
        $this->puxar = Conteiner::get("dadosIniciarBaseIntegrada");
        if(Conteiner::get('ClienteTotvsWSProjeto')){
            $this->clienteWebService = Conteiner::get(Conteiner::get('ClienteTotvsWSProjeto'));
        }else{
            $this->clienteWebService = Conteiner::get('ClienteTotvsWS');
        }
    }

    public function  executar(){
        for($i=0; $i < count($this->puxar); $i++){
            $pasta = DIR_BASE.'/file/iniciarBase/xml/'.(100 + $i);
            mkdir($pasta);
            var_dump($this->puxar[$i][0]);
            if(count($this->puxar[$i]) == 3){
                $this->limite($this->puxar[$i][0], $this->puxar[$i][2], $pasta);
            }else{
                $this->padrao($this->puxar[$i][0], $pasta);
            }
        }
    }

    private function padrao($consulta,$pasta){
        $resultado = $this->clienteWebService->realizarConsultaSQL($consulta,'0','S','');
        file_put_contents($pasta.'/'.$consulta.'.xml',$resultado);
    }

    private function limite($consulta, $qtd, $pasta){
        $inicio = $fim = 0;
        do{
            $fim = $inicio + $qtd;
            do{
                $limite = 'INICIO='.$inicio.';FIM='.$fim;
                var_dump($consulta, 'INICIO='.$inicio.';FIM='.$fim);
                $resultado = $this->clienteWebService->realizarConsultaSQL($consulta,'0','S',$limite);
            }while(trim($resultado) == '');
            $inicio = $fim;
            file_put_contents($pasta.'/'.$consulta.'_'.$fim.'.xml',$resultado);
        }while (substr_count($resultado,'/Resultado') > 0);
    }

}
