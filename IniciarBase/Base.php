<?php
namespace Rubeus\IntegracaoTotvs\IniciarBase;
use Rubeus\Bd\Persistencia;
use Rubeus\ContenerDependencia\Conteiner;

class Base {
    private $base;
    private $insert;
    
    public function __construct() {
        $this->base = $this->insert = [];
    }

    private function getTabela($tabela,$chave){
        if(!isset($this->base[$tabela])){
            if(file_exists(DIR_BASE.'/file/iniciarBase/base/'.$tabela.'.json')){
                $this->base[$tabela] = json_decode(file_get_contents(DIR_BASE.'/file/iniciarBase/base/'.$tabela.'.json'));
            }else{
                $this->base[$tabela] = new \stdClass();
            }
        }
        return $this->base[$tabela]->{$chave};
    }
    
    public function consultar($tabela,$chave){
        $id = $this->getTabela($tabela,$chave);
        return !is_null($id) ? $id : false;
    }
    
    public function inserir($tabela,$baseInsert,$chave){
        
        $insert = str_replace(';','',$baseInsert);
        if(!isset($this->insert[$tabela])){
            $this->insert[$tabela] = ['id' => 0, 'insert' => ''];
        }
        $this->insert[$tabela]['id']++;
        $insert = str_replace(strtolower('values ('),strtolower('values ( '.$this->insert[$tabela]['id'].','),$insert);
			
        if(trim($this->insert[$tabela]['insert']) == ''){
			$insert = str_replace(strtolower($tabela.'('),strtolower($tabela.'( id,'),$insert);
            $this->insert[$tabela]['insert'] .= $insert;
        }else{
            $insert = explode('values',$insert);
            $this->insert[$tabela]['insert'] .= ','.$insert[1];
        }
        $this->base[$tabela]->{$chave} = $this->insert[$tabela]['id'];
        return $this->insert[$tabela]['id'];
    }
    
    public function enviarBanco(){
        $arquivo = file_get_contents(DIR_BASE.'/file/iniciarBase/log.txt');
        $arquivoFalhou = file_get_contents(DIR_BASE.'/file/iniciarBase/logFalhou.txt');
        
       
        
        foreach($this->insert as $tabela => $valor){
            $salvou = Persistencia::execultar($this->insert[$tabela]['insert']);
            $arquivo .= $this->insert[$tabela]['insert'].';';            
                if(!$salvou){
                    $arquivoFalhou .= $this->insert[$tabela]['insert'].';';
                }
                $this->insert[$tabela]['insert']='';
        }
        
        
        file_put_contents(DIR_BASE.'/file/iniciarBase/log.txt', $arquivo);
        file_put_contents(DIR_BASE.'/file/iniciarBase/logFalhou.txt', $arquivoFalhou);
    }
    
    public function liberar(){
        foreach($this->base as $tabela => $valor){
            file_put_contents(DIR_BASE.'/file/iniciarBase/base/'.$tabela.'.json', json_encode($valor));
        }
        $this->base = [];
    }
    
}
