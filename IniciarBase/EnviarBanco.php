<?php
namespace Rubeus\IntegracaoTotvs\IniciarBase;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;

class EnviarBanco{
    private $base;

    public function __construct() {
        $this->base = new Base();
    }

    private function tratar($valor){
        return is_string($valor)? addslashes($valor) :$valor;
    }

    private function estruturar($objAtributo,$valor,$identificador,$indicePai,$naoNull){
        foreach($valor as $ch => $vl){
            if(is_array($vl)){
               $this->estruturar($objAtributo->get($ch),$vl, $identificador, $indicePai.'::'.$ch);
            }else{
                $objAtributo->set($ch,  $this->tratar($vl));
            }
        }

        $ident = $identificador[$indicePai];
        $chave = $this->identificarRegistro($objAtributo,$ident,$naoNull[$indicePai]);

        $objAtributo->set('momento',date('Y-m-d H:i:s'));
        $objAtributo->set('ativo',1);


        if(!$objAtributo->get('id') && trim($chave) != ''){
            $id = $this->base->inserir($objAtributo->getClasseMap(), $objAtributo->salvar(false,true),$chave);
            $objAtributo->set('id',$id);
        }else if(trim($chave) == ''){
            $objAtributo->set('id',null);
        }
    }

    public function salvarBanco($objeto,$identificador,$naoNull){
        foreach($objeto as $entidade => $valores){
            if(empty($valores))continue;
            $obj = ConteinerEntidade::getInstancia($entidade);
            foreach($valores as $linha){
                $obj->limparObjeto();
                $this->estruturar($obj,$linha, $identificador, $entidade,$naoNull);
            }
        }
        $this->base->enviarBanco();
        $this->base->liberar();
    }

    private function identificarRegistro($objAtributo,$ident,$naoNull){
        $chave = [];

        for($i=0;$i<count($ident);$i++){
            $valor = $objAtributo->get($ident[$i]);
            $chave[] = is_object($valor)? $valor->get('id') : $valor;
        }
        for($i=0;$i<count($naoNull);$i++){
            $valor = $objAtributo->get($naoNull[$i]);
            $valorTestar = is_object($valor)? $valor->get('id') : $valor;
           if(trim($valorTestar) == '' || $valorTestar === false || is_null($valorTestar)){
               return '';
           }
        }
        $chave = implode(';',$chave);
        $id = false;
        if(!empty($chave)){
            $id = $this->base->consultar($objAtributo->getClasseMap(),$chave);
        }
        $objAtributo->set('id',$id);
        return $chave;
    }
}
