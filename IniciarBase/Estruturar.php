<?php 
namespace Rubeus\IntegracaoTotvs\IniciarBase;
use Rubeus\ContenerDependencia\Conteiner;

class Estruturar{
    private $objeto;
    private $mapa;
    private $eliminar;
    private $indiceLimpeza;
    private $identificador;
    private $naoNull;


    public function __construct() {
        $this->objeto = array();
        $this->mapa = array();
    }
   
    public function  popular($data,$map,$nameDataServer){
        if(!empty($data)){
            $dataServer = $map->getDataServer($nameDataServer);        
            $mapa = $dataServer->getMapa();
            $this->montarObjeto($mapa, $data);
        }
    }
    
    public function getMapa(){
        return $this->mapa;
    }
    
    public function getObjeto(){
        return $this->objeto;
    }
    
    public function getNaoNull(){
        return $this->naoNull;
    }
    
    public function getIdentificador(){
        return $this->identificador;
    }
    
    private function montarObjeto($mapa, $data){
        unset($mapa['campoChave']);
        $this->identificador = $this->naoNull = $this->eliminar = array();
        $this->indiceLimpeza = [[],[],[],[]];
        foreach ($mapa as $entidade=>$valor){
            $entidade = str_replace('['.$mapa[$entidade]['attr'].']','',$entidade);
            
            if(empty($valor['attr'])){
                $indiceEntidade = explode('\\', $entidade);
                $indice = $indiceEntidade[count($indiceEntidade)-1];
                
                $this->objeto[$indice] = [];
                
                $identificador = $this->propriedade($this->objeto[$indice], $valor, $data);
                $this->identificador[$indice] = $identificador['identificador'];
                $this->naoNull[$indice] = $identificador['naoInserir'];
                
            }else{
                $attr = explode('::',rtrim($valor['attr']));
                if(count($attr)  == 2){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1]);
                    $this->identificador[$attr[0].'::'.$attr[1]] = $identificador['identificador'];
                    $this->naoNull[$attr[0].'::'.$attr[1]] = $identificador['naoInserir'];
                }else if(count($attr)  == 3){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2]] = $identificador['identificador'];
                    $this->naoNull[$attr[0].'::'.$attr[1].'::'.$attr[2]] = $identificador['naoInserir'];
                }else if(count($attr)  == 4){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2], $attr[3]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2].'::'.$attr[3]] = $identificador['identificador'];
                    $this->naoNull[$attr[0].'::'.$attr[1].'::'.$attr[2].'::'.$attr[3]] = $identificador['naoInserir'];
                }
            }
        }
        
        foreach($this->objeto as $chave=> $valor){
            for($i=0;$i<count($this->eliminar);$i++){
                switch (count($this->eliminar[$i]['eliminar'])){
                    case 1:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]]);
                        }
                        break;
                    case 2:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]]);
                        }
                        break;
                    case 3:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]]);
                        }
                        break;
                    case 4:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]][$this->eliminar[$i]['eliminar'][3]]);
                        }
                        break;
                }
            }
        }
    }
    
    
    private function propriedade(&$objeto, $mapa, $data,$indice=false, $indice2=false, $indice3=false){        
        $naoInserir = $identificador = array();        
        for($i=0;$i<count($mapa['local']); $i++){
            if(isset($data->{$mapa['totvs'][$i]['tabela']}[1])){
                $valor = array(); 
                foreach($data->{$mapa['totvs'][$i]['tabela']} as $row){
                    $valor[] = rtrim($row->{$mapa['totvs'][$i]['atributo']});
                }
            }else{               
                $valor = [rtrim($data->{$mapa['totvs'][$i]['tabela']}->{$mapa['totvs'][$i]['atributo']})];
            }
            if(!empty($mapa['valor'][$i])){
                $valor = $this->converterValores($valor, $mapa['valor'][$i]);
            }  
            $valor = $this->executarAcao($valor,$mapa['local'][$i]['acao']);
            
            for($j =0;$j < count($valor);$j++){                
                if(!$indice){
                    $objeto[$j][$mapa['local'][$i]['atributo']] =  $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j],
                                            'campo' => [$j,$mapa['local'][$i]['atributo']]];
                    }
                }else if(!$indice2){
                    $objeto[$j][$indice][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice],
                                            'campo' => [$j,$indice,$mapa['local'][$i]['atributo']] ];
                    }
                }else  if(!$indice3){
                    $objeto[$j][$indice][$indice2][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2],
                                            'campo' =>  [$j,$indice,$indice2,$mapa['local'][$i]['atributo']] ];
                    }
                }else{
                    $objeto[$j][$indice][$indice2][$indice3][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2,$indice3],
                                            'campo' =>  [$j,$indice,$indice2,$indice3,$mapa['local'][$i]['atributo']]];
                    }
                }
            }        
            if($mapa['local'][$i]['id']==1){
                $identificador[] = $mapa['local'][$i]['atributo'];
            }
            if($mapa['local'][$i]['notNull']==1){
                $naoInserir[] = $mapa['local'][$i]['atributo'];
            }
        }
        return ['identificador' => $identificador, 'naoInserir' => $naoInserir];
    }
   
    private function converterValores($valor, $mapa){
        for($i=0;$i<count($valor); $i++){
            for($j=0;$j<count($mapa);$j++){
                if(strtoupper($valor[$i])==strtoupper($mapa[$j]['ws'])){
                   $valor[$i] = $mapa[$j]['atributo'];
                   break;
                }
            }
            if(!$valor[$i] && $valor[$i] !== 0 && $valor[$i] !== '0'){
                $valor[$i] = null;
            }
        }
        return $valor;
    }
    
    private function executarAcao($valor,$acao){
        for($i=0;$i<count($valor); $i++){
            switch ($acao){
                case 'converterMinutoHora':
                    $valor[$i] = Conteiner::get('Hora')->parseString($valor[$i],'m').':00';
                    break;
                case 'telefone':
                    $valor[$i] = Conteiner::get('Telefone')->set($valor[$i])->get();
                    break;
                case 'hash-sha256':
                    $valor[$i] = hash('sha256',$valor[$i]);
                    break;
            }
        }
        return $valor;
    }
        
}
