<?php
namespace Rubeus\IntegracaoTotvs\Envio;

class ReceberObjeto{
    private $objeto;
    private $carregar;

    public function limpar(){
        $this->objeto = null;
        $this->carregar = null;
    }


    public function  addObjeto($objeto, $carregar=true){
        $this->carregar[$objeto->getEntidade()] = $carregar;
        $this->objeto[$objeto->getEntidade()] = $objeto;
    }

    public function  getPropriedade($sequenciaAcesso,$atributo){
        if(!is_array($sequenciaAcesso)){
            $sequenciaAcesso = [$sequenciaAcesso];
        }
        $indiceEntidade = explode('\\', $sequenciaAcesso[0]);
        $sequenciaAcesso[0] = $indiceEntidade[count($indiceEntidade)-1];
        if(!isset($this->objeto[$sequenciaAcesso[0]])){
            return ['er_ex'=>1];
        }
        if($this->carregar[$sequenciaAcesso[0]] && !$this->objeto[$sequenciaAcesso[0]]->getMomento()){
            $this->objeto[$sequenciaAcesso[0]]->carregar();
        }
        $objetoAcessar = $this->acessar($this->objeto[$sequenciaAcesso[0]], $sequenciaAcesso, 1);
        if($objetoAcessar->getQtdData() > 1){
            return $objetoAcessar->getData($atributo);
        }else{
            return $objetoAcessar->get($atributo);
        }
    }

    private function acessar($objeto,$indice,$i){
        if(isset($indice[$i])){
            $obj = $objeto->get($indice[$i]);
            if(is_object($obj) && $obj->getId() && !is_null($obj->getId())){
                $obj->setAtivo(1);
                 if($this->carregar[$indice[0]] && !$obj->getMomento()){
                     $obj->carregar();
                 }
            }
            return $this->acessar($obj, $indice, $i+1);
        }
        return $objeto;
    }

    public function  get(){
        return $this->objeto;
    }
}
