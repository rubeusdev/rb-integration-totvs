<?php
namespace Rubeus\IntegracaoTotvs\Envio;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class PopularObjeto{
    private $objeto;
    private $mapa;

    public function __construct() {
        $this->objeto = array();
        $this->mapa = array();
    }

    public function  popular($objeto,$baseXml,$map){
        $arrayQuantidade = $naoEliminarPai = $eliminarPai = [];
        foreach ($map as $entidade=>$mapa){
            if(trim($mapa['attr']) == ''){
                $acesso = str_replace('['.$mapa[$entidade]['attr'].']','',$entidade);
            }else{
                $acesso = $mapa['attr'];
            }
            for ($i=0; $i<count($mapa['local']); $i++) {

                if ($mapa['local'][$i]['naoSobrescreverDadoTotvs'] == 1 && $baseXml->{$mapa['totvs'][$i]['tabela']} && $baseXml->{$mapa['totvs'][$i]['tabela']}->{$mapa['totvs'][$i]['atributo']}) {
                    break;
                }

                $chave = in_array($mapa['totvs'][$i]['atributo'], $map['campoChave']['atributo']);

                $atributo = $objeto->getPropriedade(explode('::',$acesso),$mapa['local'][$i]['atributo']);


                if(is_object($atributo)){
                    $atributo = $atributo->getId();
                }
                if(is_array($atributo) && $atributo['er_ex']){
                    break;
                }
                if(trim($mapa['totvs'][$i]['tabela']) == ''){
                    continue;
                }
                if($mapa['local'][$i]['atributoReserva'] && trim($mapa['local'][$i]['atributoReserva']) != ''){
                    $atributoReserva = $objeto->getPropriedade(explode('::',$acesso),$mapa['local'][$i]['atributoReserva']);
                }else{
                    $atributoReserva = false;
                }

                $valorEnviar = $this->tratarDado($this->converterValores($atributo, $mapa['valor'][$i],$chave,
                                        $baseXml->{$mapa['totvs'][$i]['tabela']}->{$mapa['totvs'][$i]['atributo']},$mapa['local'][$i], $atributoReserva), $mapa['local'][$i]);
                $valorEnviar = !is_array($valorEnviar) ? [$valorEnviar] : $valorEnviar;

                if((trim($valorEnviar[0]) == '' || trim($valorEnviar[0]) == '[[REMOVER]]' ||  $valorEnviar[0] === 0) && $mapa['local'][$i]['naoEnviarPadraoPai'] == 1){
                    $eliminarPai[$mapa['totvs'][$i]['tabela'].'::'.$mapa['totvs'][$i]['atributo']] = $mapa['totvs'][$i]['tabela'];
                    //break;
                }else{
                    $naoEliminarPai[] = $mapa['totvs'][$i]['tabela'].'::'.$mapa['totvs'][$i]['atributo'];

                    if(is_array($baseXml->{$mapa['totvs'][$i]['tabela']})){
                        $baseTabela = $baseXml->{$mapa['totvs'][$i]['tabela']}[0];
                    }else{
                        $baseTabela = $baseXml->{$mapa['totvs'][$i]['tabela']};
                        $baseXml->{$mapa['totvs'][$i]['tabela']} = [$baseTabela];
                    }

                    if (strpos($valorEnviar[0], '[') === 0) {
                        $verificarJson = json_decode($valorEnviar[0]);
                        if (!is_null($verificarJson) && is_array($verificarJson)) {
                            $valorEnviar = $verificarJson;
                        }
                    }
                    
                    for($j=0;$j<count($valorEnviar);$j++){
                        if(!isset($baseXml->{$mapa['totvs'][$i]['tabela']}[$j])){
                            $baseXml->{$mapa['totvs'][$i]['tabela']}[$j] = $baseTabela;
                        }
                        
                        if(trim($mapa['local'][$i]['acaoEnvio']) == 'apelido' && 
                                trim($baseXml->{$mapa['totvs'][$i]['tabela']}[$j]->NOME) !== '' && 
                                (!isset($baseXml->{$mapa['totvs'][$i]['tabela']}[$j]->APELIDO) ||
                                    trim($baseXml->{$mapa['totvs'][$i]['tabela']}[$j]->APELIDO) === '') ){
                            $apelido = explode(" ",trim($baseXml->{$mapa['totvs'][$i]['tabela']}[$j]->NOME));
                            $valorEnviar[$j] = $apelido[0];
                        }

                        $baseXml->{$mapa['totvs'][$i]['tabela']}[$j]->{$mapa['totvs'][$i]['atributo']} = $valorEnviar[$j];
                    }

                }
            }
        }


        foreach($eliminarPai as $chave => $valor){
            if(!in_array($chave, $naoEliminarPai)){
                unset($baseXml->{$valor});
            }
        }

        if(isset($baseXml->GPERMIS)){
            unset($baseXml->GPERMIS);
        }
        if(isset($baseXml->GUSRPERFIL)){
            unset($baseXml->GUSRPERFIL);
        }
        
        if(isset($baseXml->VPCompl)){
            unset($baseXml->VPCompl);
        }

        $this->limpar($baseXml, $arrayQuantidade);
    }

    private function tratarData($valor){
        $dataHora = Conteiner::get('DataHora',false);
        if(is_array($valor)){
            for($i=0;$i<count($valor);$i++){
                if($dataHora->set($valor[$i])->dataValida()){
                    $valor[$i] = str_replace(' ','T',$dataHora->get('Y-m-d H:i:s'));
                }
            }
        }else{
            if($dataHora->set($valor)->dataValida()){
                $valor = str_replace(' ','T',$dataHora->get('Y-m-d H:i:s'));
            }
        }
        return $valor;
    }

    private function tratarTelefone($valor){
        $telefone = Conteiner::get('Telefone');
        if(is_array($valor)){
            for($i=0;$i<count($valor);$i++){
                if($valor[$i] !== '[[REMOVER]]'){
                    $valor[$i] = $telefone->set($valor[$i])->colocarMascara()->get();
                }
            }
        }else{
            if($valor !== '[[REMOVER]]'){
                $valor = $telefone->set($valor)->colocarMascara()->get();
            }
        }
        return $valor;
    }

    private function converterHoraMinuto($valor){
        $hora = Conteiner::get('Hora');
        if(is_array($valor)){
            for($i=0;$i<count($valor);$i++){
                if($valor[$i] !== '[[REMOVER]]' && strpos($valor[$i],':')){
                    $valor[$i] = $hora->parseFloat($valor[$i],'m');
                }
            }
        }else{
            if($valor !== '[[REMOVER]]' && strpos($valor,':')){
                $valor = $hora->parseFloat($valor,'m');
            }
        }
        return $valor;
    }


    private function tratarDado($valor,$map){

        switch ($map['acaoEnvio']){
            case 'tratarData':
                return $this->tratarData($valor);
            case 'telefone':
                return $this->tratarTelefone($valor);
            case 'removerMascaraTelefone':
                if(is_array($valor)){
                    for($i=0;$i<count($valor);$i++){
                        if($valor[$i] !== '[[REMOVER]]'){
                            $valor[$i] = str_replace('-', '', str_replace('(', '', str_replace(')', '', str_replace(' ', '', $valor[$i]))));
                        }
                    }
                } else {
                    if($valor !== '[[REMOVER]]'){
                        $valor = str_replace('-', '', str_replace('(', '', str_replace(')', '', str_replace(' ', '', $valor))));
                    }
                }
                return $valor;
            case 'converterHoraMinuto':
                return $this->converterHoraMinuto($valor);
            case 'senha':
                $valor[$i] = hash('sha256',$valor[$i]);
                break;
            case 'macaraValorVirgula':                
                if(is_array($valor)){
                    for($i=0;$i<count($valor);$i++){
                        if($valor[$i] !== '[[REMOVER]]'){
                            $valor[$i] = str_replace('.', ',', $valor[$i]);
                        }
                    }
                } else {
                    if($valor !== '[[REMOVER]]'){
                        $valor = str_replace('.', ',', $valor);
                    }
                }
                return $valor;
                break;
            case 'apelido':
                $array = explode(" ",$valor);
                return $array[0];
                break;
    
        }
        $encoding = mb_internal_encoding();
        if($map['uppercase'] == 1){
            if(is_array($valor)){
                for($i=0;$i<count($valor);$i++){
                    $valor[$i] = mb_strtoupper($valor[$i], $encoding);
                }
            }else{
                $valor = mb_strtoupper($valor, $encoding);
            }
        }
        return $valor;
    }

    private function limpar($baseXml, $arrayQuantidade){
        $array = $eliminarTabela = array();

        foreach ($baseXml as $tabela => $base){

            if(!$baseXml->{$tabela} instanceof \stdClass){
                if(is_array($baseXml->{$tabela})){
                    $qtd = count($baseXml->{$tabela});
                    for($i=0; $i < $qtd; $i++){
                        if(!$baseXml->{$tabela}[$i] instanceof \stdClass){
                            foreach ($baseXml->{$tabela}[$i]->children() as $atributo => $valor){
                                if(isset($baseXml->{$tabela}[$i]->{$atributo}[1])){
                                    $qtd2 = count($baseXml->{$tabela}[$i]->{$atributo});
                                    for($j=0; $j < $qtd2; $j++){
                                        if(trim($baseXml->{$tabela}[$i]->{$atributo}[$j]) == '[[REMOVER]]'){
                                            $array[]=[$tabela,$i,$atributo,$j];
                                        }
                                        if($j>0){
                                            unset($baseXml->{$tabela}[$i]->{$atributo}[$j]);
                                        }
                                    }
                                }else if(trim($baseXml->{$tabela}[$i]->{$atributo}) == '[[REMOVER]]'){
                                    $array[]=[$tabela,$i,$atributo];
                                }

                            }
                            foreach ($baseXml->{$tabela}[0]->children() as $atributo => $valor){
                                if(!isset($baseXml->{$tabela}[$i]->{$atributo})){
                                    $baseXml->{$tabela}[$i]->{$atributo} = $valor;
                                }
                            }
                            if(trim($baseXml->{$tabela}[$i]->children()) == ''){
                                $eliminarTabela[] = [$tabela,$i];
                            }
                        }
                    }
                }else{
                    foreach ($baseXml->{$tabela}->children() as $atributo => $valor){
                        if(trim($baseXml->{$tabela}->{$atributo}) == '[[REMOVER]]'){
                            $array[]=[$tabela,$atributo];
                        }
                    }
                    if(trim($baseXml->{$tabela}->children()) == ''){
                        $eliminarTabela[] = [$tabela];
                    }
                }
            }
        }
        for($i=0;$i<count($array);$i++){
            $this->limparVerdade($baseXml,$array[$i]);
        }

        for($i=0;$i<count($eliminarTabela);$i++){
            if($qtd==1){
                unset($baseXml->{$eliminarTabela[$i][0]});
            }else if($qtd==2){
                unset($baseXml->{$eliminarTabela[$i][0]}[$eliminarTabela[$i][1]]);
            }
        }
    }

    private function limparVerdade($baseXml, $array){
        $qtd = count($array);
        if($qtd==2){
            unset($baseXml->{$array[0]}->{$array[1]});
        }else if($qtd==3){
            unset($baseXml->{$array[0]}[$array[1]]->{$array[2]});
        }else if($qtd==4){
            unset($baseXml->{$array[0]}[$array[1]]->{$array[2]}[$array[3]]);
        }
    }

    private function converterValores($valor, $mapa,$chave,$valorAtual,$mapaLocal,$valorOpcional){

        if(trim($mapaLocal['default']) != ''){
            $valorAtual = $mapaLocal['default'] ;
        }else if($valorOpcional && (!$valorAtual || trim($valorAtual) == '' || trim($valorAtual) == '[[REMOVER]]' )){
            $valorAtual = $valorOpcional;
        }
        if($chave && (!$valor || empty($valor)) && trim($mapaLocal['naoEnviarPadrao']) != 1){
            $valor = trim($valorAtual) == '[[REMOVER]]' ?  0 : $valorAtual;
        }
        if((!$valor || empty($valor))  &&  trim($mapaLocal['naoEnviarPadrao']) == 1){
            $valor = '[[REMOVER]]' ;
        }
        if(empty($mapa)){
            if(((!$valor || empty($valor)) && $valor !== 0  && $valor !== '0') && trim($mapaLocal['naoEnviarPadrao']) != 1){
                return  trim($valorAtual) == '[[REMOVER]]' ?
                                            (trim($mapaLocal['naoEnviarPadrao']) != 1? 0 : '[[REMOVER]]' )
                                            : $valorAtual;
            }
           return $valor;
        }
        if(!is_array($valor)){
            $valor = [$valor];
        }
        for($i=0;$i<count($valor); $i++){
            for($j=0;$j<count($mapa);$j++){
                if($valor[$i]===0) $valor[$i] = '0';
                if($valor[$i]==$mapa[$j]['atributo'] || $valor[$i] == '*'){
                   if($valor[$i] != '$ws'){
                        if($mapa[$j]['ws'] == 'null'){
                            $valor[$i] = null;
                        }
                        $valor[$i] = $mapa[$j]['ws'];
                   }
                   break;
                }
            }
        }
        return $valor;
    }
}
