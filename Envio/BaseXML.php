<?php
namespace Rubeus\IntegracaoTotvs\Envio;
use Rubeus\ContenerDependencia\Conteiner;

class BaseXML{
    private $baseEnviarTotvs;

    public function addBase($xml, $dataServer){
        if(!isset($this->baseEnviarTotvs[$dataServer])){
            $this->baseEnviarTotvs[$dataServer] = array();
        }

        if(Conteiner::get('LeitorXMLTotvsPadrao')){
            $leitorXML = Conteiner::get(Conteiner::get('LeitorXMLTotvsPadrao'));
        }else{
            $leitorXML = Conteiner::get('LeitorXMLTotvsProjeto');
        }
        
        $resposta = $leitorXML->lerData($xml, false,'readRecord');

        $objetoBase = simplexml_load_string($resposta);
        $parseObj = str_replace('</xs:',"</xs",str_replace('<xs:',"<xs",$resposta));

        $ob= simplexml_load_string($parseObj);
        $this->montar($objetoBase,$ob);
        $this->baseEnviarTotvs[$dataServer][] = $objetoBase;

        return $objetoBase;
    }

    public function montar($objetoBase,$ob){
        foreach($ob->xsschema->xselement->xscomplexType->xschoice->xselement as $tabela){
            if(rtrim($tabela['name']) !== 'SResponsavelContrato' && rtrim($tabela['name']) !== 'GPERMIS' && rtrim($tabela['name']) !== 'GUSRPERFIL'){
                $objetoBase->addChild(rtrim($tabela['name']),'');
                foreach($tabela->xscomplexType->xssequence->xselement as $coluna){
                    $valorPadrao = rtrim($coluna['default']) != '' ? rtrim($coluna['default']) : '[[REMOVER]]';
                    $objetoBase->{rtrim($tabela['name'])}->addChild(rtrim($coluna['name']),$valorPadrao);
                }
            }
        }
    }

    public function getXML($dataServer){
        $array = array();

        for($i=0;$i<count($this->baseEnviarTotvs[$dataServer]);$i++){
            $array[] = str_replace('<xs:element name="LIVEID" msdata:ReadOnly="true" msdata:Caption="Usu&#xE1;rio do Microsoft Live" minOccurs="0">
                  <xs:simpleType>
                    <xs:restriction base="xs:string">
                      <xs:maxLength value="150"/>
                    </xs:restriction>
                  </xs:simpleType>
                </xs:element>', '', $this->baseEnviarTotvs[$dataServer][$i]->asXML());
            }
        return $array;
    }

    public function limpar(){
        return $this->baseEnviarTotvs = [];
    }

    public function getObj($dataServer){
        return $this->baseEnviarTotvs[$dataServer];
    }

    public function get(){
        return $this->baseEnviarTotvs;
    }
}
