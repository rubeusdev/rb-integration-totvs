<?php
namespace Rubeus\IntegracaoTotvs\Entrada;

class RetornoConsulta{

    public function  montarRetorno($data){        
        if(!empty($data)){
            $resultado = $linha = array();
            foreach ($data as $chaveTabela=>$tabela){
                $linha = [];
                foreach ($tabela as $chave=>$valor){
                    $linha[$chave] = trim($valor);
                }
                $resultado[] = $linha;
            }
            return $resultado;
        }
        return false;
    }
}
