<?php 
namespace Rubeus\IntegracaoTotvs\Entrada;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

class EnviarBanco{
    private $objeto;
    private $mapa;
    private $eliminar;
    private $naoInserir;


    public function __construct() {
        $this->objeto = array();
        $this->mapa = array();
    }
    
    private function tratar($valor){
        return is_string($valor)? addslashes($valor) :$valor;
    }
        
    private function estruturar($objAtributo,$valor,$identificador,$indicePai ){
        foreach($valor as $ch => $vl){                            
            if(is_array($vl)){
               $this->estruturar($objAtributo->get($ch),$vl, $identificador, $indicePai.'::'.$ch);
            }else{
                $objAtributo->set($ch,  $this->tratar($vl));
            }
        }
        $ident = $identificador[$indicePai];
        $this->teste($objAtributo,$ident);

        if(!$objAtributo->get('momento')){
            $objAtributo->set('momento',date('Y-m-d H:i:s'));
        }
        $objAtributo->set('ativo',1);
        
        if($this->naoInserir[$indicePai] != 1 || intval($objAtributo->getId()) > 0){
            $objAtributo->salvar();
        }
    }
    
    private function salvarBanco($objeto,$identificador){
        $i=0;
        foreach($objeto as $entidade => $valores){
            $i++;
            $obj = ConteinerEntidade::getInstancia($entidade);            
            foreach($valores as $linha){
                $obj->limparObjeto();
                $this->estruturar($obj,$linha, $identificador, $entidade);
            }
            if($i==25){
                Persistencia::commit();
                $i=0;
             }
        }
         Persistencia::commit();
    }
    
    private function teste($objAtributo,$ident){
        for($i=0;$i<count($ident);$i++){  
            $colunas[$i] = $objAtributo->getXml()->getColuna($ident[$i]);
            $valores[$i] = $objAtributo->get($ident[$i]);

            if(is_object($valores[$i])){
                $objAtributo->filtro($colunas[$i]." = '".$valores[$i]->get('id')."'");
            }else{
                $objAtributo->filtro($colunas[$i]." = '".$valores[$i]."'");
            }
        }
        if(is_array($colunas) && !empty($colunas)){
            $resultado = $objAtributo->carregar('id, '.implode(',',$colunas), false);
        }        
        $objAtributo->limparFiltro();
        $id = false;
        $qtd = count($resultado);
        for($k=0;$k<$qtd;$k++){            
            if(count($valores) == $this->numCompativel($resultado,$valores, $colunas, $k)){
                $id = $resultado[$k]['id'];
                break;
            }
        }
        if($id){
            $objAtributo->set('id',$id);
        }else{
            $objAtributo->set('id',false);
        }
    }
    
    private function numCompativel($resultado,$valores, $colunas, $k){        
        $econtrei = 0;
        $qtd = count($valores);
        for($j=0;$j<$qtd;$j++){           
            if(is_object($valores[$j])){
                if($resultado[$k][$colunas[$j]] == $valores[$j]->get('id')){
                    $econtrei++;
                }
            }else{
                if($resultado[$k][$colunas[$j]] == $valores[$j]){
                    $econtrei++;
                }
            }
        }
        return $econtrei;
    }
    
    public function  popular($data,$map,$nameDataServer){
        if(!empty($data)){
            $dataServer = $map->getDataServer($nameDataServer);        
            $mapa = $dataServer->getMapa();
            $this->montarObjeto($mapa, $data);
            $this->salvarBanco($this->objeto, $this->identificador);
            
            $this->objeto = array();
            $this->mapa = array();
        }
    }
    
    public function getMapa(){
        return $this->mapa;
    }
    
    private function montarObjeto($mapa, $data){
        $reveter = 0;
        unset($mapa['campoChave']);
        $this->eliminar = array();
        foreach ($mapa as $entidade=>$valor){
            $entidade = str_replace('['.$mapa[$entidade]['attr'].']','',$entidade);
            if(isset($valor['filtro']) && !empty($valor['filtro'])){
                $registrarData = clone $data;
                $reveter = 1;
                foreach($valor['filtro'] as $filtro){                    
                    $acesso = explode('.',$filtro['campo']);
                    if(isset($data->{$acesso[0]}[1])){  
                        for($i=0;$i<count($data->{$acesso[0]});$i++){
                            if(rtrim($data->{$acesso[0]}[$i]->{$acesso[1]}) != $filtro['valor']){
                                unset($data->{$acesso[0]}[$i]);
                                $i--;
                            } 
                        }
                    }else if(rtrim($data->{$acesso[0]}->{$acesso[1]}) != $filtro['valor']){
                        unset($data->{$acesso[0]});                          
                    }
                }
                if(empty($data->{$acesso[0]})){
                    $reveter = 0;
                    $data = $registrarData;
                    continue;
                }
            }
            if(empty($valor['attr'])){
                $indiceEntidade = explode('\\', $entidade);
                $indice = $indiceEntidade[count($indiceEntidade)-1];
                
                $this->objeto[$indice] = [];
                
                $identificador = $this->propriedade($this->objeto[$indice], $valor, $data);
                $this->identificador[$indice] = $identificador;
                $this->naoInserir[$indice] =  rtrim($valor['naoInserir']);
                
            }else{
                $attr = explode('::',rtrim($valor['attr']));
                if(count($attr)  == 2){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1]);
                    $this->identificador[$attr[0].'::'.$attr[1]] = $identificador;
                    $this->naoInserir[$attr[0].'::'.$attr[1]] = rtrim($valor['naoInserir']);
                }else if(count($attr)  == 3){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2]] = $identificador;
                    $this->naoInserir[$attr[0].'::'.$attr[1].'::'.$attr[2]] = rtrim($valor['naoInserir']);
                }else if(count($attr)  == 4){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2], $attr[3]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2].'::'.$attr[3]] = $identificador;
                    $this->naoInserir[$attr[0].'::'.$attr[1].'::'.$attr[2].'::'.$attr[3]] = rtrim($valor['naoInserir']);
                }
            }
            if($reveter == 1){
                $reveter = 0;
                $data = $registrarData;
            }
        }
        
        foreach($this->objeto as $chave=> $valor){
            for($i=0;$i<count($this->eliminar);$i++){
                switch (count($this->eliminar[$i]['eliminar'])){
                    case 1:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]]);
                        }
                        break;
                    case 2:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]]);
                        }
                        break;
                    case 3:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]]);
                        }
                        break;
                    case 4:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]][$this->eliminar[$i]['eliminar'][3]]);
                        }
                        break;
                }
            }
        }
    }
    
    private function propriedade(&$objeto, $mapa, $data,$indice=false, $indice2=false, $indice3=false){        
        $identificador = array();        
        for($i=0;$i<count($mapa['local']); $i++){
            if(isset($data->{$mapa['totvs'][$i]['tabela']}[1])){
                $valor = array(); 
                foreach($data->{$mapa['totvs'][$i]['tabela']} as $row){
                    $valor[] = rtrim($row->{$mapa['totvs'][$i]['atributo']});
                }
            }else{               
                $valor = [rtrim($data->{$mapa['totvs'][$i]['tabela']}->{$mapa['totvs'][$i]['atributo']})];
            }
            if(!empty($mapa['valor'][$i])){
                $valor = $this->converterValores($valor, $mapa['valor'][$i]);
            }  
            $valor = $this->executarAcao($valor,$mapa['local'][$i]['acao']);
            for($j =0;$j < count($valor);$j++){                
                if(!$indice){
                    $objeto[$j][$mapa['local'][$i]['atributo']] =  $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j],
                                            'campo' => [$j,$mapa['local'][$i]['atributo']]];
                    }
                }else if(!$indice2){
                    $objeto[$j][$indice][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice],
                                            'campo' => [$j,$indice,$mapa['local'][$i]['atributo']] ];
                    }
                }else  if(!$indice3){
                    $objeto[$j][$indice][$indice2][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2],
                                            'campo' =>  [$j,$indice,$indice2,$mapa['local'][$i]['atributo']] ];
                    }
                }else{
                    $objeto[$j][$indice][$indice2][$indice3][$mapa['local'][$i]['atributo']] = $valor[$j];
                    if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                        $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2,$indice3],
                                            'campo' =>  [$j,$indice,$indice2,$indice3,$mapa['local'][$i]['atributo']]];
                    }
                }
            }        
            if($mapa['local'][$i]['id']==1){
                $identificador[] = $mapa['local'][$i]['atributo'];
            }
        }
        return $identificador;
    }
   
    private function converterValores($valor, $mapa){
        for($i=0;$i<count($valor); $i++){
            for($j=0;$j<count($mapa);$j++){
                if(strtoupper($valor[$i])==strtoupper($mapa[$j]['ws'])){
                   $valor[$i] = $mapa[$j]['atributo'];
                   break;
                }
            }
        }
        return $valor;
    }
    
    private function executarAcao($valor,$acao){
        for($i=0;$i<count($valor); $i++){
            if(is_null($valor[$i]) || trim($valor[$i]) == '' || !$valor[$i] || !isset($valor[$i])){
                if($acao == 'valorValido'){
                    $valor[$i] = false;
                }
                continue;  
            } 
            switch ($acao){
                case 'converterMinutoHora':
                    $valor[$i] = Conteiner::get('Hora',false)->parseString($valor[$i],'m').':00';
                    break;
                case 'data':
                    if(strpos($valor[$i],':')){
                        $valor[$i] = str_replace('T',' ',$valor[$i]);
                        $valor[$i] = Conteiner::get('DataHora',false)->set($valor[$i],'Y-m-d H:i:s')->get('Y-m-d H:i:s');
                    }else{
                        $valor[$i] = Conteiner::get('DataHora',false)->set($valor[$i],'Y-m-d')->get('Y-m-d');
                    }
                    break;
                case 'telefone': 
                    $valor[$i] = Conteiner::get('Telefone',false)->set($valor[$i])->get();
                    break;
                case 'hash-sha256':
                    $valor[$i] = hash('sha256',$valor[$i]);
                    break;
            }
        }
        return $valor;
    }  
}
