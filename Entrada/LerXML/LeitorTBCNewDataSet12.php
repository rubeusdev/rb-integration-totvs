<?php
namespace Rubeus\IntegracaoTotvs\Entrada\LerXML;

class LeitorTBCNewDataSet12{

    public function  lerData($xml, $conteudoXML=true,$metodo=false){
        $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', ':s'],'',$xml);
        $conteudo = str_replace(['body'],"Body", str_replace(['</s:','</soap:'],"</",str_replace(['<s:','<soap:'],"<",$xmlValido)));

        $resposta = simplexml_load_string($conteudo, 'SimpleXMLElement', LIBXML_PARSEHUGE);

        if($conteudoXML){
            switch($metodo){
                case 'realizarConsultaSQL':
                    if($resposta->Body->RealizarConsultaSQLResponse){
                        $xmlResposta = rtrim($resposta->Body->RealizarConsultaSQLResponse->RealizarConsultaSQLResult);
                    }else{
                        $xmlResposta = rtrim($resposta->Body->RealizarConsultaSQLAuthResponse->RealizarConsultaSQLAuthResult);
                    }
                    return simplexml_load_string($xmlResposta);
                case 'readRecord':
                    if($resposta->Body->ReadRecordResponse){
                        $xmlResposta = rtrim($resposta->Body->ReadRecordResponse->ReadRecordResult);
                    }else{
                        $xmlResposta = rtrim($resposta->Body->ReadRecordAuthResponse->ReadRecordAuthResult);
                    }
                    return simplexml_load_string($xmlResposta);
                case 'saveRecord':
                    if($resposta->Body->SaveRecordResponse){
                        return $resposta->Body->SaveRecordResponse->SaveRecordResult;
                    }else{
                        return $resposta->Body->SaveRecordAuthResponse->SaveRecordAuthResult;
                    }
            }
        }
        switch($metodo){
            case 'realizarConsultaSQL':
                if($resposta->Body->RealizarConsultaSQLResponse){
                    return $resposta->Body->RealizarConsultaSQLResponse->RealizarConsultaSQLResult;
                }else{
                    return $resposta->Body->RealizarConsultaSQLAuthResponse->RealizarConsultaSQLAuthResult;
                }
            case 'readRecord':
                if($resposta->Body->ReadRecordResponse){
                    return rtrim($resposta->Body->ReadRecordResponse->ReadRecordResult);
                }else{
                    return rtrim($resposta->Body->ReadRecordAuthResponse->ReadRecordAuthResult);
                }
            case 'saveRecord':
                if($resposta->Body->SaveRecordResponse){
                    return rtrim($resposta->Body->SaveRecordResponse->SaveRecordResult);
                }else{
                    return rtrim($resposta->Body->SaveRecordAuthResponse->SaveRecordAuthResult);
                }
        }
    }

}
