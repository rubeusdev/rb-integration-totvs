<?php 
namespace Rubeus\IntegracaoTotvs\Entrada\LerXML;

class LeitorTBCNewDataSet{
   
    public function  lerData($xml, $conteudoXML=true,$metodo=false){       
        $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;',':s'],'',$xml);
        $conteudo = str_replace(['</s:'],"</",str_replace(['<s:'],"<",$xmlValido));
        
        $resposta = simplexml_load_string($conteudo);
	
        if($conteudoXML){            
            switch($metodo){
                case 'realizarConsultaSQL': 
                    $xmlResposta = rtrim($resposta->Body->RealizarConsultaSQLResponse->RealizarConsultaSQLResult);
                    return simplexml_load_string($xmlResposta);
                case 'readRecord': 
                    $xmlResposta = rtrim($resposta->Body->ReadRecordResponse->ReadRecordResult);
                    return simplexml_load_string($xmlResposta);
                case 'saveRecord': 
                    return $resposta->Body->SaveRecordResponse->SaveRecordResult;
            }
        }		
        switch($metodo){
            case 'realizarConsultaSQL': 
                return $resposta->Body->RealizarConsultaSQLResponse->RealizarConsultaSQLResult;
            case 'readRecord': 
                return rtrim($resposta->Body->ReadRecordResponse->ReadRecordResult);
            case 'saveRecord': 
                return rtrim($resposta->Body->SaveRecordResponse->SaveRecordResult);
        }
    }

}
