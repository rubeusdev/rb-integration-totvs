<?php
namespace Rubeus\IntegracaoTotvs\Entrada\LerXML;

class LeitorTBCAuthDiffgr{

    public function  lerData($xml,$conteudoXML=true,$metodo=false){
        $xmlValido = str_replace(['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;'],'',$xml);
        $conteudo = str_replace(['</soap:','</diffgr:'],"</",str_replace(['<soap:','<diffgr:'],"<",$xmlValido));

        $resposta = simplexml_load_string($conteudo);

	if($conteudoXML){
            switch($metodo){
                case 'realizarConsultaSQL':
                    return $resposta->Body->RealizarConsultaSQLAuthResponse->RealizarConsultaSQLAuthResult->diffgram->DocumentElement->Resultado;
                case 'readRecord':
                    $xmlResposta = rtrim($resposta->Body->ReadRecordAuthResponse->ReadRecordAuthResult);
                    return simplexml_load_string($xmlResposta);
                case 'saveRecord':
                    return $resposta->Body->SaveRecordAuthResponse->SaveRecordAuthResult;
            }
        }

        switch($metodo){
            case 'realizarConsultaSQL':
                return $resposta->Body->RealizarConsultaSQLAuthResponse->RealizarConsultaSQLAuthResult->diffgram->DocumentElement->asXml();
            case 'readRecord':
                return rtrim($resposta->Body->ReadRecordAuthResponse->ReadRecordAuthResult);
            case 'saveRecord':
                return  rtrim($resposta->Body->SaveRecordAuthResponse->SaveRecordAuthResult);
        }

    }

}
