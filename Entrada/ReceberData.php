<?php
namespace Rubeus\IntegracaoTotvs\Entrada;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class ReceberData{
    private $data;

    public function  lerData($xml,$id=false,$registrar=true, $conteudoXML=true,$metodo=false){
        if(Conteiner::get('LeitorXMLTotvsPadrao')){
            $leitorXML = Conteiner::get(Conteiner::get('LeitorXMLTotvsPadrao'));
        }else{
            $leitorXML = Conteiner::get('LeitorXMLTotvsProjeto');
        }
        
        $resposta = $leitorXML->lerData($xml, $conteudoXML,$metodo);

        if($conteudoXML){
            if($registrar && $id){
                $this->data[$id] = $resposta;
            }
            return array('data'=>$resposta,'id'=>$id);
        }
        return $resposta;
    }

    public function  getData($id){
        if(isset($this->data[$id])){
            return $this->data[$id];
        }
        return false;
    }

    public function  getPropriedade($id, $tabela, $propriedade){
        if(!isset($this->data[$id]->{$tabela}[1])){
            return rtrim($this->data[$id]->{$tabela}->{$propriedade});
        }
        $valor = array();
        for($i=0;$i<count($this->data[$id]->{$tabela});$i++){
            $valor[] =  rtrim($this->data[$id]->{$tabela}[$i]->{$propriedade});
        }
        return $valor;
    }
}
