<?php 
namespace Rubeus\IntegracaoTotvs\Entrada;

class PopularObjeto{
    private $objeto;
    private $mapa;
    
    public function __construct() {
        $this->objeto = array();
        $this->mapa = array();
    }
    
    public function  popular($data,$map,$nameDataServer){
        $dataServer = $map->getDataServer($nameDataServer);        
        $mapa = $dataServer->getMapa();
        $this->montarObjeto($mapa, $data);
        return $this->objeto;
    }
    
    public function getMapa(){
        return $this->mapa;
    }
    
    private function montarObjeto($mapa, $data){
        $reveter = 0;
        unset($mapa['campoChave']);  
        
        foreach ($mapa as $entidade=>$valor){
            if(isset($valor['filtro'])){
                $registrarData = clone $data;
                $reveter = 1;
                foreach($valor['filtro'] as $filtro){                    
                    $acesso = explode('.',$filtro['campo']);
                    if(isset($data->{$acesso[0]}[1])){
                        for($i=0;$i<count($data->{$acesso[0]});$i++){
                            if(rtrim($data->{$acesso[0]}[$i]->{$acesso[1]}) != $filtro['valor']){
                                unset($data->{$acesso[0]}[$i]);
                            } 
                        }
                    }else if(rtrim($data->{$acesso[0]}->{$acesso[1]}) != $filtro['valor']){
                       unset($data->{$acesso[0]});                          
                    }
                }
            }
            if(empty($valor['attr'])){
                $indiceEntidade = explode('\\', $entidade);
                $indice = $indiceEntidade[count($indiceEntidade)-1];
                
                $this->objeto[$indice] = new $entidade();
                
                $identificador = $this->propriedade($this->objeto[$indice], $valor, $data);
                $this->mapa[0][] = ['entidade'=>$entidade, 'entidadeOrigem'=>$indice,
                                    'atributo'=>[],'identificador'=>$identificador];
                
            }else{
                $attr = explode('::',rtrim($valor['attr']));
                
                $this->objeto[$attr[0]]->set($attr[1],new $entidade());
                
                $identificador = $this->propriedade($this->objeto[$attr[0]]->get($attr[1]), $valor, $data);
                $posicaoMapa = $this->localizarMapa($attr[0]);
                $posicaoMapa['atributo'] = $attr[1];
                
                $this->mapa[$posicaoMapa['nivel']][] = ['entidade'=>$entidade,
                                                        'entidadeOrigem'=>$posicaoMapa['entidade'],
                                                        'atributo'=> $posicaoMapa['atributo'],
                                                        'identificador'=>$identificador];
            }
            if($reveter == 1){
                $reveter = 0;
                $data = $registrarData;
            }
        }
    }
    
    private function propriedade($objeto, $mapa, $data){
        $valorSetar = $identificador = array();
        
        for($i=0;$i<count($mapa['local']); $i++){           
            if(isset($data->{$mapa['totvs'][$i]['tabela']}[1])){
                $valor = array(); 
                foreach($data->{$mapa['totvs'][$i]['tabela']} as $row){
                    $valor[] = rtrim($row->{$mapa['totvs'][$i]['atributo']});
                }
            }else{               
                $valor = [rtrim($data->{$mapa['totvs'][$i]['tabela']}->{$mapa['totvs'][$i]['atributo']})];
            }
            if(!empty($mapa['valor'][$i])){
                $valor = $this->converterValores($valor, $mapa['valor'][$i]);
            }           
            $valorSetar[] = array( 'atributo' => $mapa['local'][$i]['atributo'],
                                   'id' => $mapa['local'][$i]['id'],
                                   'valor' => $valor);
            
            if($mapa['local'][$i]['id']==1){
                $identificador[] = $mapa['local'][$i]['atributo'];
            }
        }
        
        $valores = $this->limparRepetido($valorSetar);
        
        for($i=0;$i<count($valores); $i++){ 
            $objeto->set($valores[$i]['atributo'],$valores[$i]['valor']);            
        }
        
        return $identificador;
    }
    
    private function limparRepetido($valorSetar){
        $qtd = count($valorSetar[0]['valor']);
        $qtdAtributo = count($valorSetar);
        
        for($i=0;$i<$qtd; $i++){
            if(!isset($valorSetar[0]['valor'][$i])){
                continue;
            }
            $chaveBusca = [];            
            for($j=0;$j<$qtdAtributo;$j++){
                if($valorSetar[$j]['id'] == 1){
                    $chaveBusca[$valorSetar[$j]['atributo']]  = $valorSetar[$j]['valor'][$i];
                }
            }
            for($j=$i+1;$j<$qtd;$j++){
                if(!isset($valorSetar[0]['valor'][$j])){
                    continue;
                }
                $chave = $igualdade = 0;
                for($k=0;$k<$qtdAtributo;$k++){
                    if($valorSetar[$k]['id'] == 1){
                        $chave++;
                        if($chaveBusca[$valorSetar[$k]['atributo']] == $valorSetar[$k]['valor'][$j]){
                            $igualdade++;
                        }else{
                            break;
                        }
                    }
                }
                if($chave == $igualdade){
                    for($k=0;$k<$qtdAtributo;$k++){
                        unset($valorSetar[$k]['valor'][$j]);
                    }
                }
            }            
        }
        for($k=0;$k<$qtdAtributo;$k++){
            ksort($valorSetar[$k]['valor']);
        }
        return $valorSetar;
    }
    
    private function converterValores($valor, $mapa){
        for($i=0;$i<count($valor); $i++){
            for($j=0;$j<count($mapa);$j++){
                if($valor[$i]==$mapa[$j]['ws']){
                   $valor[$i] = $mapa[$j]['atributo'];
                   break;
                }
            }
            if(!$valor[$i] && $valor[$i] !== 0 && $valor[$i] !== '0'){
                $valor[$i] = null;
            }
        }
        return $valor;
    }
    
    private function localizarMapa($entidade){
        for($i=0;$i<count($this->mapa); $i++){            
            for($j=0;$j<count($this->mapa[$i]);$j++){
                $indiceEntidade = explode('\\', $this->mapa[$i][$j]['entidade']);
                $indice = $indiceEntidade[count($indiceEntidade)-1];
                if($indice==$entidade){
                   return [ 'atributo' => $this->mapa[$i][$j]['atributo'],
                            'nivel' => $i+1,
                            'entidade' => $this->mapa[$i][$j]['entidadeOrigem'] ];
                } 
            }
        }
    }
    
}
