<?php 
namespace Rubeus\IntegracaoTotvs\Entrada;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;

class LimparBase{
    private $objeto;
    private $mapa;
    private $eliminar;


    public function __construct() {
        $this->objeto = array();
        $this->mapa = array();
        $this->eliminar = array();
    }
    
    private function tratar($valor){
        return is_string($valor)? str_replace("'","\'",$valor) :$valor;
    }
    /*
    private function salvarBanco($objeto,$identificador){
        foreach($objeto as $entidade => $valores){
            $obj = ConteinerEntidade::getInstancia($entidade);            
            foreach($valores as $linha){
                $obj->limparObjeto();
                foreach($linha as $chave => $valor){
                    if(is_array($valor)){
                        $objAtributo = $obj->get($chave);
                        foreach($valor as $ch => $vl){                            
                            if(is_array($vl)){
                                $objAtributoDois = $objAtributo->get($ch);
                                foreach($vl as $c => $v){                            
                                    $objAtributoDois->set($c,  $this->tratar($v));
                                }
                                $ident = $identificador[$entidade.'::'.$chave.'::'.$ch];
                                $this->teste($objAtributoDois,$ident['id'],$ident['del']);

                            }else{
                                $objAtributo->set($ch,  $this->tratar($vl));
                            }
                        }
                        $ident = $identificador[$entidade.'::'.$chave];
                        $this->teste($objAtributo,$ident['id'],$ident['del']);
                       
                    }else{                    
                        $obj->set($chave, $this->tratar($valor));                    
                    }              
                }
                $ident = $identificador[$entidade];
                $this->teste($obj,$ident['id'],$ident['del']);
                
            }
        }        
    }
    */
    private function estruturar($objAtributo,$valor,$identificador,$indicePai ){
        foreach($valor as $ch => $vl){                            
            if(is_array($vl)){
               $this->estruturar($objAtributo->get($ch),$vl, $identificador, $indicePai.'::'.$ch);
            }else{
                $objAtributo->set($ch,  $this->tratar($vl));
            }
        }
        $ident = $identificador[$indicePai];
        $this->teste($objAtributo,$ident['id'],$ident['del']);
    }
    
    private function salvarBanco($objeto,$identificador){
        foreach($objeto as $entidade => $valores){
            $obj = ConteinerEntidade::getInstancia($entidade);            
            foreach($valores as $linha){
                $obj->limparObjeto();
                $this->estruturar($obj,$linha, $identificador, $entidade);
            }
        }        
    }
    
    private function teste($objAtributo,$ident,$deletar){
        for($i=0;$i<count($ident);$i++){  
            $colunas[$i] = $objAtributo->getXml()->getColuna($ident[$i]);
            $valores[$i] = $objAtributo->getData($ident[$i]);
            
            if(empty($valores[$i])){
                $valores[$i] = $objAtributo->get($ident[$i]);
            }
            
            if(is_array($valores[$i])){
                $objAtributo->filtro($colunas[$i]." in(".implode(',',$valores[$i]).")");
            }else if(is_object($valores[$i])){
                $obj = $valores[$i];
                $valores[$i] = $obj->getData('id');
                if(empty($valores[$i])){
                    $valores[$i] = $obj->get('id');
                }
                if(is_array($valores[$i])){
                    $objAtributo->filtro($colunas[$i]." in(".implode(',',$valores[$i]).")");
                }else{
                    $objAtributo->filtro($colunas[$i]." = '".$valores[$i]."'");
                }
            }else{
                $objAtributo->filtro($colunas[$i]." = '".$valores[$i]."'");
            }
        }
        if($deletar == 1){
            $objAtributo->deletar();
        }
        if(is_array($colunas) && !empty($colunas)){
            $resultado = $objAtributo->carregar('id, '.implode(',',$colunas), false);
        }        
        $objAtributo->limparFiltro();
        $id = [];
        for($k=0;$k<count($resultado);$k++){            
            $id[] = $resultado[$k]['id'];
        }
        if($id){
            $objAtributo->set('id',$id);
        }
       
    }
    
    private function numCompativel($resultado,$valores, $colunas, $k){        
        $econtrei = 0;
        for($j=0;$j<count($valores);$j++){           
            if(is_object($valores[$j])){
                if($resultado[$k][$colunas[$j]] == $valores[$j]->get('id')){
                    $econtrei++;
                }
            }else{
                if($resultado[$k][$colunas[$j]] == $valores[$j]){
                    $econtrei++;
                }
            }
        }
        return $econtrei;
    }
       
    public function  popular($campo,$map,$nameDataServer){
        $dataServer = $map->getDataServer($nameDataServer);        
        $mapa = $dataServer->getMapa();
        $this->montarObjeto($mapa, $campo);
        //var_dump($this->objeto, $this->identificador);
        $this->salvarBanco($this->objeto, $this->identificador);
        $this->objeto = array();
        $this->mapa = array();
        $this->eliminar = array();
    }
    
    public function getMapa(){
        return $this->mapa;
    }
    
    private function montarObjeto($mapa, $data){       
        unset($mapa['campoChave']);
        foreach ($mapa as $entidade=>$valor){
            $entidade = str_replace('['.$mapa[$entidade]['attr'].']','',$entidade);
            
            if(empty($valor['attr'])){
                $indiceEntidade = explode('\\', $entidade);
                $indice = $indiceEntidade[count($indiceEntidade)-1];
                
                $this->objeto[$indice] = [];
                
                $identificador = $this->propriedade($this->objeto[$indice], $valor, $data);
                $this->identificador[$indice] = ['id' => $identificador, 'del' => $valor['deletar'] ];
                
            }else{
                $attr = explode('::',rtrim($valor['attr']));
                if(count($attr)  == 2){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1]);
                    $this->identificador[$attr[0].'::'.$attr[1]] = 
                                ['id' => $identificador, 'del' => $valor['deletar']];
                }else if(count($attr)  == 3){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2]] = 
                                ['id' => $identificador, 'del' => $valor['deletar']];
                }else if(count($attr)  == 4){
                    $identificador = $this->propriedade($this->objeto[$attr[0]], $valor, $data, $attr[1], $attr[2], $attr[3]);
                    $this->identificador[$attr[0].'::'.$attr[1].'::'.$attr[2].'::'.$attr[3]] = ['id' => $identificador, 'del' => $valor['deletar']];
                }
            }            
        }
        
        foreach($this->objeto as $chave=> $valor){
            for($i=0;$i<count($this->eliminar);$i++){
                switch (count($this->eliminar[$i]['eliminar'])){
                    case 1:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]]);
                        }
                        break;
                    case 2:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]]);
                        }
                        break;
                    case 3:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]]);
                        }
                        break;
                    case 4:
                        if(!isset($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) ||
                               (trim($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]) == '' &&
                                !is_array($this->objeto[$chave][$this->eliminar[$i]['campo'][0]][$this->eliminar[$i]['campo'][1]][$this->eliminar[$i]['campo'][2]][$this->eliminar[$i]['campo'][3]][$this->eliminar[$i]['campo'][4]]))){
                            unset($this->objeto[$chave][$this->eliminar[$i]['eliminar'][0]][$this->eliminar[$i]['eliminar'][1]][$this->eliminar[$i]['eliminar'][2]][$this->eliminar[$i]['eliminar'][3]]);
                        }
                        break;
                }
            }
        }
    }
    
    private function propriedade(&$objeto, $mapa, $campo,$indice=false, $indice2=false, $indice3=false){        
        $identificador = array();        
        for($i=0;$i<count($mapa['local']); $i++){
            if($mapa['local'][$i]['id']==1){
                $valor = [$campo[$mapa['totvs'][$i]['atributo']]];
                if(!empty($mapa['valor'][$i])){
                    $valor = $this->converterValores($valor, $mapa['valor'][$i]);
                }            
                for($j =0;$j < count($valor);$j++){                
                     if(!$indice){
                        $objeto[$j][$mapa['local'][$i]['atributo']] =  $valor[$j];
                        if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                            $this->eliminar[] = ['eliminar' => [$j],
                                                'campo' => [$j,$mapa['local'][$i]['atributo']]];
                        }
                    }else if(!$indice2){
                        $objeto[$j][$indice][$mapa['local'][$i]['atributo']] = $valor[$j];
                        if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                            $this->eliminar[] = ['eliminar' => [$j,$indice],
                                                'campo' => [$j,$indice,$mapa['local'][$i]['atributo']] ];
                        }
                    }else  if(!$indice3){
                        $objeto[$j][$indice][$indice2][$mapa['local'][$i]['atributo']] = $valor[$j];
                        if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                            $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2],
                                                'campo' =>  [$j,$indice,$indice2,$mapa['local'][$i]['atributo']] ];
                        }
                    }else{
                        $objeto[$j][$indice][$indice2][$indice3][$mapa['local'][$i]['atributo']] = $valor[$j];
                        if(trim($valor[$j]) == '' && $mapa['local'][$i]['notNull'] == 1){
                            $this->eliminar[] = ['eliminar' => [$j,$indice,$indice2,$indice3],
                                                'campo' =>  [$j,$indice,$indice2,$indice3,$mapa['local'][$i]['atributo']]];
                        }
                    }
                }       
            
                $identificador[] = $mapa['local'][$i]['atributo'];
            }
        }
        return $identificador;
    }
   
    private function converterValores($valor, $mapa){
        for($i=0;$i<count($valor); $i++){
            for($j=0;$j<count($mapa);$j++){
                if($valor[$i]==$mapa[$j]['ws']){
                   $valor[$i] = $mapa[$j]['atributo'];
                   break;
                }
            }
        }
        return $valor;
    }
        
}
