<?php 
namespace Rubeus\IntegracaoTotvs\Entrada;

class PersistirObjeto{
    
    public function  persistir($objeto, $mapa){        
        for($i=count($mapa)-1;$i>=0; $i--){
            for($j=0;$j<count($mapa[$i]);$j++){
                $objetoAtual = $this->getObjeto($objeto[$mapa[$i][$j]['entidadeOrigem']], $mapa[$i][$j]['atributo']);
                $this->identificar($objetoAtual, $mapa[$i][$j]['identificador']);
                
                $objetoAtual->salvar();                 
            }
        }
    }
    
    private function getObjeto($objeto, $atributo){
        $obj = $objeto;
        if(trim($atributo)!=='' && !empty($atributo)){
            $obj = $obj->get($atributo);
        }
        return $obj; 
    }
    
    private function percorrerValor($objeto,$identificador){
        if($objeto->getQtdData()>0){
            $valor = [];
            for($i=0;$i<$objeto->getQtdData();$i++){
               $objeto->percorrer($i); 
               $valor[] = $objeto->get($identificador);
            } 
            return $valor;
        }        
        return  $objeto->get($identificador);
    }
    
    public function  identificar($objeto, $identificador){
        $colunas = array();
        for($i=0;$i<count($identificador);$i++){  
            $colunas[$i] = $objeto->getXml()->getColuna($identificador[$i]);
            $valores[$i] = $this->percorrerValor($objeto,$identificador[$i]);
            
            if(is_array($valores[$i])){
                $objeto->filtro( $colunas[$i]." in ('".implode("','",$valores[$i])."')");
            }else{
                $objeto->filtro($colunas[$i].' = '.$valores[$i]);
            } 
        }
        
        if(is_array($colunas) && !empty($colunas)){
            $resultado = $objeto->carregar('id, '.implode(',',$colunas), false);
        }
        $objeto->limparFiltro();
        $id = array();
        for($i=0;$i<count($valores[0]);$i++){
            $id[$i] = false;
            for($k=0;$k<count($resultado);$k++){            
                if(count($valores) == $this->numCompativel($resultado,$valores, $colunas, $k, $i)){
                    $id[$i] = $resultado[$k]['id'];
                    break;
                }
            }
        }
        
        if($id){
            $objeto->set('id',$id);
        }
    }
    
    private function numCompativel($resultado,$valores, $colunas, $k, $i){        
        $econtrei = 0;
        for($j=0;$j<count($valores);$j++){           
            if($resultado[$k][$colunas[$j]] == $valores[$j][$i]){
                $econtrei++;
            }
        }
        return $econtrei;
    }
    
}
