
        
CREATE TABLE `statuschamadaintegracaototvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `titulo` varchar(45)   ,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `registrarchamadawebservicetotvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `inicio` datetime   ,
    `fim` datetime   ,
    `momento` datetime   ,
    `ativo` tinyint   ,
    `processo` varchar(255)   ,
    `etapa` varchar(255)   ,
    `processototvschamada` varchar(255)   ,
    `usuario` varchar(255)   ,
    `formulavisual` varchar(255)   ,
    `codsistema` varchar(255)   ,
    `antesatualizar` varchar(255)   ,
    `codigoacao` varchar(255)   ,
    `processototvs_id` INT   ,
    INDEX `registrarchamadawebservicetotvs_fk_processototvs_id_idx`(`processototvs_id` ASC),
    CONSTRAINT `registrarchamadawebservicetotvs_fk_processototvs_id` 
         FOREIGN KEY (`processototvs_id`) REFERENCES `processototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `momentochamada` varchar(255)   ,
    `processototvsidentificador` tinyint   ,
    `parametro1` varchar(255)   ,
    `parametro2` varchar(255)   ,
    `parametro3` varchar(255)   ,
    `parametro4` varchar(255)   ,
    `parametro5` varchar(255)   ,
    `parametro6` varchar(255)   ,
    `parametro7` varchar(255)   ,
    `parametro8` varchar(255)   ,
    `parametro9` varchar(255)   ,
    `parametro10` varchar(255)   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `errochamadatotvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `textoerro` varchar(255)   ,
    `textoerrogeral` varchar(255)   ,
    `tipoerro` INT   ,
    `relevancia` INT   ,
    `momento` datetime   ,
    `ativo` tinyint   ,
    `registrarchamadatotvs_id` INT   ,
    INDEX `errochamadatotvs_fk_registrarchamadatotvs_id_idx`(`registrarchamadatotvs_id` ASC),
    CONSTRAINT `errochamadatotvs_fk_registrarchamadatotvs_id` 
         FOREIGN KEY (`registrarchamadatotvs_id`) REFERENCES `registrarchamadatotvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `registrarchamadatotvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `inicio` datetime   ,
    `fim` datetime   ,
    `momento` datetime   ,
    `ativo` tinyint   ,
    `metodo` varchar(255)   ,
    `dataserver` varchar(255)   ,
    `filtro` varchar(255)   ,
    `disponibilidade` tinyint   ,
    `resultado` tinyint   ,
    `enviadoaplicacaoreal` tinyint   ,
    `contexto` varchar(255)   ,
    `parametro` varchar(255)   ,
    `campo` varchar(255)   ,
    `resposta` varchar(255)   ,
    `envio` varchar(255)   ,
    `debug` varchar(255)   ,
    `registrarchamadawebservicetotvs_id` INT   ,
    INDEX `registrarchamadatotvs_fk_registrarchamadawebservicetotvs_id_idx`(`registrarchamadawebservicetotvs_id` ASC),
    CONSTRAINT `registrarchamadatotvs_fk_registrarchamadawebservicetotvs_id` 
         FOREIGN KEY (`registrarchamadawebservicetotvs_id`) REFERENCES `registrarchamadawebservicetotvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `processototvs_id` INT   ,
    INDEX `registrarchamadatotvs_fk_processototvs_id_idx`(`processototvs_id` ASC),
    CONSTRAINT `registrarchamadatotvs_fk_processototvs_id` 
         FOREIGN KEY (`processototvs_id`) REFERENCES `processototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `statuschamadaintegracaototvs_id` INT   ,
    INDEX `registrarchamadatotvs_fk_statuschamadaintegracaototvs_id_idx`(`statuschamadaintegracaototvs_id` ASC),
    CONSTRAINT `registrarchamadatotvs_fk_statuschamadaintegracaototvs_id` 
         FOREIGN KEY (`statuschamadaintegracaototvs_id`) REFERENCES `statuschamadaintegracaototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `agendaprocesso_id` INT   ,
    INDEX `registrarchamadatotvs_fk_agendaprocesso_id_idx`(`agendaprocesso_id` ASC),
    CONSTRAINT `registrarchamadatotvs_fk_agendaprocesso_id` 
         FOREIGN KEY (`agendaprocesso_id`) REFERENCES `agendaprocesso` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `origemprocessototvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `titulo` varchar(45)   ,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `processototvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `titulo` varchar(45)   ,
    `descricao` varchar(105)   ,
    `origemprocessototvs_id` INT   ,
    INDEX `processototvs_fk_origemprocessototvs_id_idx`(`origemprocessototvs_id` ASC),
    CONSTRAINT `processototvs_fk_origemprocessototvs_id` 
         FOREIGN KEY (`origemprocessototvs_id`) REFERENCES `origemprocessototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        
CREATE TABLE `monitoramentototvs`( 
    `id` int  AUTO_INCREMENT ,
     PRIMARY KEY (`id`),
    `statuschamadaintegracaototvs_id` INT   ,
    INDEX `monitoramentototvs_fk_statuschamadaintegracaototvs_id_idx`(`statuschamadaintegracaototvs_id` ASC),
    CONSTRAINT `monitoramentototvs_fk_statuschamadaintegracaototvs_id` 
         FOREIGN KEY (`statuschamadaintegracaototvs_id`) REFERENCES `statuschamadaintegracaototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `momentochamada` datetime   ,
    `origemprocessototvs_id` INT   ,
    INDEX `monitoramentototvs_fk_origemprocessototvs_id_idx`(`origemprocessototvs_id` ASC),
    CONSTRAINT `monitoramentototvs_fk_origemprocessototvs_id` 
         FOREIGN KEY (`origemprocessototvs_id`) REFERENCES `origemprocessototvs` (`id`)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION,
    `momento` datetime   ,
    `ativo` tinyint   ) ENGINE=InnoDB DEFAULT CHARSET=latin1;