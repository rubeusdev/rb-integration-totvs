<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class ProcessoTotvs extends Persistente{
        private $id = false;
        private $titulo = false;
        private $descricao = false;
        private $origemProcessoTotvs = false;
        private $momento = false;
        private $ativo = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getTitulo() {
            return $this->titulo;
        }

        public function setTitulo($titulo) {
            $this->titulo = $titulo;
        } 
                
        public function getDescricao() {
            return $this->descricao;
        }

        public function setDescricao($descricao) {
            $this->descricao = $descricao;
        } 
            
        public function getOrigemProcessoTotvs() {
            if(!$this->origemProcessoTotvs)
                    $this->origemProcessoTotvs = new OrigemProcessoTotvs(); 
            return $this->origemProcessoTotvs;
        }

        public function setOrigemProcessoTotvs($origemProcessoTotvs) {
            if($origemProcessoTotvs instanceof OrigemProcessoTotvs)
                $this->origemProcessoTotvs = $origemProcessoTotvs;
            else $this->getOrigemProcessoTotvs()->setId($origemProcessoTotvs);
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        }
        
    }