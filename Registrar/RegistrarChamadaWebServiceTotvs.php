<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class RegistrarChamadaWebServiceTotvs extends Persistente{
        private $id = false;
        private $inicio = false;
        private $fim = false;
        private $momento = false;
        private $ativo = false;
        private $processo = false;
        private $etapa = false;
        private $processoTotvsChamada = false;
        private $usuario = false;
        private $formulaVisual = false;
        private $codSistema = false;
        private $antesAtualizar = false;
        private $codigoAcao = false;
        private $processoTotvs = false;
        private $momentoChamada = false;
        private $processoTotvsIdentificador = false;
        private $parametro1 = false;
        private $parametro2 = false;
        private $parametro3 = false;
        private $parametro4 = false;
        private $parametro5 = false;
        private $parametro6 = false;
        private $parametro7 = false;
        private $parametro8 = false;
        private $parametro9 = false;
        private $parametro10 = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getInicio() {
            return $this->inicio;
        }

        public function setInicio($inicio) {
            $this->inicio = $inicio;
        } 
                
        public function getFim() {
            return $this->fim;
        }

        public function setFim($fim) {
            $this->fim = $fim;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        } 
                
        public function getProcesso() {
            return $this->processo;
        }

        public function setProcesso($processo) {
            $this->processo = $processo;
        } 
                
        public function getEtapa() {
            return $this->etapa;
        }

        public function setEtapa($etapa) {
            $this->etapa = $etapa;
        } 
                
        public function getProcessoTotvsChamada() {
            return $this->processoTotvsChamada;
        }

        public function setProcessoTotvsChamada($processoTotvsChamada) {
            $this->processoTotvsChamada = $processoTotvsChamada;
        } 
                
        public function getUsuario() {
            return $this->usuario;
        }

        public function setUsuario($usuario) {
            $this->usuario = $usuario;
        } 
                
        public function getFormulaVisual() {
            return $this->formulaVisual;
        }

        public function setFormulaVisual($formulaVisual) {
            $this->formulaVisual = $formulaVisual;
        } 
                
        public function getCodSistema() {
            return $this->codSistema;
        }

        public function setCodSistema($codSistema) {
            $this->codSistema = $codSistema;
        } 
                
        public function getAntesAtualizar() {
            return $this->antesAtualizar;
        }

        public function setAntesAtualizar($antesAtualizar) {
            $this->antesAtualizar = $antesAtualizar;
        } 
                
        public function getCodigoAcao() {
            return $this->codigoAcao;
        }

        public function setCodigoAcao($codigoAcao) {
            $this->codigoAcao = $codigoAcao;
        } 
            
        public function getProcessoTotvs() {
            if(!$this->processoTotvs)
                    $this->processoTotvs = new ProcessoTotvs(); 
            return $this->processoTotvs;
        }

        public function setProcessoTotvs($processoTotvs) {
            if($processoTotvs instanceof ProcessoTotvs)
                $this->processoTotvs = $processoTotvs;
            else $this->getProcessoTotvs()->setId($processoTotvs);
        } 
                
        public function getMomentoChamada() {
            return $this->momentoChamada;
        }

        public function setMomentoChamada($momentoChamada) {
            $this->momentoChamada = $momentoChamada;
        } 
                
        public function getProcessoTotvsIdentificador() {
            return $this->processoTotvsIdentificador;
        }

        public function setProcessoTotvsIdentificador($processoTotvsIdentificador) {
            $this->processoTotvsIdentificador = $processoTotvsIdentificador;
        } 
                
        public function getParametro1() {
            return $this->parametro1;
        }

        public function setParametro1($parametro1) {
            $this->parametro1 = $parametro1;
        } 
                
        public function getParametro2() {
            return $this->parametro2;
        }

        public function setParametro2($parametro2) {
            $this->parametro2 = $parametro2;
        } 
                
        public function getParametro3() {
            return $this->parametro3;
        }

        public function setParametro3($parametro3) {
            $this->parametro3 = $parametro3;
        } 
                
        public function getParametro4() {
            return $this->parametro4;
        }

        public function setParametro4($parametro4) {
            $this->parametro4 = $parametro4;
        } 
                
        public function getParametro5() {
            return $this->parametro5;
        }

        public function setParametro5($parametro5) {
            $this->parametro5 = $parametro5;
        } 
                
        public function getParametro6() {
            return $this->parametro6;
        }

        public function setParametro6($parametro6) {
            $this->parametro6 = $parametro6;
        } 
                
        public function getParametro7() {
            return $this->parametro7;
        }

        public function setParametro7($parametro7) {
            $this->parametro7 = $parametro7;
        } 
                
        public function getParametro8() {
            return $this->parametro8;
        }

        public function setParametro8($parametro8) {
            $this->parametro8 = $parametro8;
        } 
                
        public function getParametro9() {
            return $this->parametro9;
        }

        public function setParametro9($parametro9) {
            $this->parametro9 = $parametro9;
        } 
                
        public function getParametro10() {
            return $this->parametro10;
        }

        public function setParametro10($parametro10) {
            $this->parametro10 = $parametro10;
        }
        
    }