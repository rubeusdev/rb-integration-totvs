<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class MonitoramentoTotvs extends Persistente{
        private $id = false;
        private $statusChamadaIntegracaoTotvs = false;
        private $momentoChamada = false;
        private $origemProcessoTotvs = false;
        private $momento = false;
        private $ativo = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
            
        public function getStatusChamadaIntegracaoTotvs() {
            if(!$this->statusChamadaIntegracaoTotvs)
                    $this->statusChamadaIntegracaoTotvs = new StatusChamadaIntegracaoTotvs(); 
            return $this->statusChamadaIntegracaoTotvs;
        }

        public function setStatusChamadaIntegracaoTotvs($statusChamadaIntegracaoTotvs) {
            if($statusChamadaIntegracaoTotvs instanceof StatusChamadaIntegracaoTotvs)
                $this->statusChamadaIntegracaoTotvs = $statusChamadaIntegracaoTotvs;
            else $this->getStatusChamadaIntegracaoTotvs()->setId($statusChamadaIntegracaoTotvs);
        } 
                
        public function getMomentoChamada() {
            return $this->momentoChamada;
        }

        public function setMomentoChamada($momentoChamada) {
            $this->momentoChamada = $momentoChamada;
        } 
            
        public function getOrigemProcessoTotvs() {
            if(!$this->origemProcessoTotvs)
                    $this->origemProcessoTotvs = new OrigemProcessoTotvs(); 
            return $this->origemProcessoTotvs;
        }

        public function setOrigemProcessoTotvs($origemProcessoTotvs) {
            if($origemProcessoTotvs instanceof OrigemProcessoTotvs)
                $this->origemProcessoTotvs = $origemProcessoTotvs;
            else $this->getOrigemProcessoTotvs()->setId($origemProcessoTotvs);
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        }
        
    }