<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class OrigemProcessoTotvs extends Persistente{
        private $id = false;
        private $titulo = false;
        private $momento = false;
        private $ativo = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getTitulo() {
            return $this->titulo;
        }

        public function setTitulo($titulo) {
            $this->titulo = $titulo;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        }
        
    }