<?php 
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;

class ConsultarPrimeiraChamada{

    public function consultar($usuario, $codSistema,$codigoAcao){
        $query = Conteiner::getInstancia('Query',false);
        $query->select('id')->add("parametro1")->add("parametro2")->add("parametro3")
                ->add("parametro4")->add("parametro5")->add("parametro6")->add("parametro7")
                ->add("parametro8")->add("parametro9")->add("parametro10");
        
        $query->from('registrarchamadawebservicetotvs');
        
        $query->addVariaveis([$usuario, $codSistema, $codigoAcao])
                ->where('usuario = ?')->add('codsistema = ?')
                ->add('codigoacao = ?')->add('ativo = 1');  
        $query->order('id','desc');
        return $query->executar('A');
    }
    
    public function desativar($id){
        $registrarChamada = ConteinerEntidade::getInstancia('RegistrarChamadaWebServiceTotvs');
        $registrarChamada->setId($id);
        $registrarChamada->deletar();
    }
}  
