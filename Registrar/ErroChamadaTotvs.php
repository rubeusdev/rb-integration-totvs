<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class ErroChamadaTotvs extends Persistente{
        private $id = false;
        private $textoErro = false;
        private $textoErroGeral = false;
        private $tipoErro = false;
        private $relevancia = false;
        private $momento = false;
        private $ativo = false;
        private $registroChamadaTotvs = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getTextoErro() {
            return $this->textoErro;
        }

        public function setTextoErro($textoErro) {
            $this->textoErro = $textoErro;
        } 
                
        public function getTextoErroGeral() {
            return $this->textoErroGeral;
        }

        public function setTextoErroGeral($textoErroGeral) {
            $this->textoErroGeral = $textoErroGeral;
        } 
                
        public function getTipoErro() {
            return $this->tipoErro;
        }

        public function setTipoErro($tipoErro) {
            $this->tipoErro = $tipoErro;
        } 
                
        public function getRelevancia() {
            return $this->relevancia;
        }

        public function setRelevancia($relevancia) {
            $this->relevancia = $relevancia;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        } 
            
        public function getRegistroChamadaTotvs() {
            if(!$this->registroChamadaTotvs)
                    $this->registroChamadaTotvs = new RegistrarChamadaTotvs(); 
            return $this->registroChamadaTotvs;
        }

        public function setRegistroChamadaTotvs($registroChamadaTotvs) {
            if($registroChamadaTotvs instanceof RegistrarChamadaTotvs)
                $this->registroChamadaTotvs = $registroChamadaTotvs;
            else $this->getRegistroChamadaTotvs()->setId($registroChamadaTotvs);
        }
        
    }