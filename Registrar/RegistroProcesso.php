<?php 
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade;
use Rubeus\Bd\Persistencia;
use Rubeus\ContenerDependencia\Conteiner;

class RegistroProcesso{

    public function registrar($processo){
               
        if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        }
        
        $processoTotvs = ConteinerEntidade::getInstancia('ProcessoTotvs');
        $processoTotvs->setTitulo($processo);
        $processoTotvs->carregar('id');
        if(!$processoTotvs->getId()){
            $processoTotvs->setMomento(date('H-m-d H:i:s'));
            $processoTotvs->setAtivo(1);
            $processoTotvs->salvar();            
        }
        
        if (Persistencia::getBaseAtual() != "principal") {
            Persistencia::mudarBase("principal");
        }
        
        return $processoTotvs->getId();
    }
    
}  
