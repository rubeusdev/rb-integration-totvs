<?php
        
namespace Rubeus\IntegracaoTotvs\Registrar;
use Rubeus\ORM\Persistente as Persistente;

    class RegistroComunicacaoWebService extends Persistente{
        private $id = false;
        private $inicio = false;
        private $fim = false;
        private $momento = false;
        private $ativo = false;
        private $registrarChamadaWebServiceTotvs = false;
        private $processoTotvsIdentificador = false;
        private $statusChamadaIntegracaoTotvs = false;
        private $agendaProcesso = false;
        private $envio = false;
        private $resposta = false;
        private $debug = false; 
                
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        } 
                
        public function getInicio() {
            return $this->inicio;
        }

        public function setInicio($inicio) {
            $this->inicio = $inicio;
        } 
                
        public function getFim() {
            return $this->fim;
        }

        public function setFim($fim) {
            $this->fim = $fim;
        } 
                
        public function getMomento() {
            return $this->momento;
        }

        public function setMomento($momento) {
            $this->momento = $momento;
        } 
                
        public function getAtivo() {
            return $this->ativo;
        }

        public function setAtivo($ativo) {
            $this->ativo = $ativo;
        } 
            
        public function getRegistrarChamadaWebServiceTotvs() {
            if(!$this->registrarChamadaWebServiceTotvs)
                    $this->registrarChamadaWebServiceTotvs = new RegistrarChamadaWebServiceTotvs(); 
            return $this->registrarChamadaWebServiceTotvs;
        }

        public function setRegistrarChamadaWebServiceTotvs($registrarChamadaWebServiceTotvs) {
            if($registrarChamadaWebServiceTotvs instanceof RegistrarChamadaWebServiceTotvs)
                $this->registrarChamadaWebServiceTotvs = $registrarChamadaWebServiceTotvs;
            else $this->getRegistrarChamadaWebServiceTotvs()->setId($registrarChamadaWebServiceTotvs);
        } 
            
        public function getProcessoTotvsIdentificador() {
            if(!$this->processoTotvsIdentificador)
                    $this->processoTotvsIdentificador = new ProcessoTotvs(); 
            return $this->processoTotvsIdentificador;
        }

        public function setProcessoTotvsIdentificador($processoTotvsIdentificador) {
            if($processoTotvsIdentificador instanceof ProcessoTotvs)
                $this->processoTotvsIdentificador = $processoTotvsIdentificador;
            else $this->getProcessoTotvsIdentificador()->setId($processoTotvsIdentificador);
        } 
            
        public function getStatusChamadaIntegracaoTotvs() {
            if(!$this->statusChamadaIntegracaoTotvs)
                    $this->statusChamadaIntegracaoTotvs = new StatusChamadaIntegracaoTotvs(); 
            return $this->statusChamadaIntegracaoTotvs;
        }

        public function setStatusChamadaIntegracaoTotvs($statusChamadaIntegracaoTotvs) {
            if($statusChamadaIntegracaoTotvs instanceof StatusChamadaIntegracaoTotvs)
                $this->statusChamadaIntegracaoTotvs = $statusChamadaIntegracaoTotvs;
            else $this->getStatusChamadaIntegracaoTotvs()->setId($statusChamadaIntegracaoTotvs);
        } 
            
        public function getAgendaProcesso() {
            if(!$this->agendaProcesso)
                    $this->agendaProcesso = new \Rubeus\FilaProcesso\Dominio\AgendaProcesso(); 
            return $this->agendaProcesso;
        }

        public function setAgendaProcesso($agendaProcesso) {
            if($agendaProcesso instanceof \Rubeus\FilaProcesso\Dominio\AgendaProcesso)
                $this->agendaProcesso = $agendaProcesso;
            else $this->getAgendaProcesso()->setId($agendaProcesso);
        } 
                
        public function getEnvio() {
            return $this->envio;
        }

        public function setEnvio($envio) {
            $this->envio = $envio;
        } 
                
        public function getResposta() {
            return $this->resposta;
        }

        public function setResposta($resposta) {
            $this->resposta = $resposta;
        } 
                
        public function getDebug() {
            return $this->debug;
        }

        public function setDebug($debug) {
            $this->debug = $debug;
        }
        
    }