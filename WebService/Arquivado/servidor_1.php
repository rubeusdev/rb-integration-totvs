<?php
ini_set('display_errors',0);
error_reporting(-1);

use Rubeus\Processo\Aplicacao\Entrada;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;

function entradaTotvs($processo,$etapa,$processoTotvs,$usuario,$codigoSistema,$formulaVisual,$antesAtualizar,$codigoAcao, 
                        $parametro1, $parametro2, $parametro3, $parametro4, $parametro5, $parametro6,
                        $parametro7, $parametro8, $parametro9, $parametro10){
    
    if($processoTotvs == 'Disponibilidade'){
        return true;
    }
    
    if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
        Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
    }
    
    $filaProcesso = Conteiner::get('FilaProcesso');
    $processoFila = Conteiner::get('ProcessoFila');
    $processoFila->criar($processo,$etapa,1,PROJETOREGISTROATIVIDADE);
    
    $processoFila->addParametro('ProcessoTotvs',$processoTotvs);
    $processoFila->addParametro('Processo',$processo);
    $processoFila->addParametro('Usuario',$usuario);
    $processoFila->addParametro('Etapa',$etapa);
    $processoFila->addParametro('CodigoAcao',$codigoAcao);
    $processoFila->addParametro('CodigoSistema',$codigoSistema);
    $processoFila->addParametro('FormulaVisual',$formulaVisual);
    //$processoFila->addParametro('Momento',$momento);
    $processoFila->addParametro('Parametro1',$parametro1);
    $processoFila->addParametro('Parametro2',$parametro2);
    $processoFila->addParametro('Parametro3',$parametro3);
    $processoFila->addParametro('Parametro4',$parametro4);
    $processoFila->addParametro('Parametro5',$parametro5);
    $processoFila->addParametro('Parametro6',$parametro6);
    $processoFila->addParametro('Parametro7',$parametro7);
    $processoFila->addParametro('Parametro8',$parametro8);
    $processoFila->addParametro('Parametro9',$parametro9);
    $processoFila->addParametro('Parametro10',$parametro10);
    $processoFila->addParametro('AntesAtualizar',$antesAtualizar);
    $filaProcesso->add($processoFila);
    
    $filaProcesso->registrar();
    Persistencia::commit();
    
    return true;
    
}

ini_set("soap.wsdl_cache_enabled", "0");

$endereco = DOMINIO_PROJETO."/wsdl?root=integracaoTotvs";

$server = new SoapServer($endereco, array('encoding'=>'ISO-8859-1'));
$server->addFunction("entradaTotvs");
$server->handle();
