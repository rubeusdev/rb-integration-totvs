<?php
namespace Rubeus\IntegracaoTotvs\WebService;

use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\IntegracaoTotvs\Registrar\RegistrarChamadaTotvs;
use Rubeus\Bd\Persistencia;
use Rubeus\Arquivos\Arquivo;

require_once(\Ambiente::dir(__DIR__).'/../lib/nusoap.php');


class LocalTotvsWS
{
    private $clienteWS = false;
    private $ws = 'wsDataServer/MEX?wsdl';
    private $wsConsulta = 'wsConsultaSQL/MEX?wsdl';
    private $wsEdu = 'wsEdu/MEX?wsdl';
    private $wsReport = 'wsReport/MEX?wsdl';
    private $aplicacao;
    private $inicio;
    private $fim;
    private $registrar;

    public function __construct($aplicacao)
    {
        $this->aplicacao = $aplicacao;
        $this->registrar = new RegistrarChamadaTotvs();
    }

    private function conectar($ws=1, $usuario=false, $senha=false)
    {
        if ($this->clienteWS) {
            return;
        }
        if (!$usuario) {
            $aplicacoes = json_decode(file_get_contents(DIR_BASE.'/'.DEFINICAO_APLICACAO_INTEGRADA));
            if (!isset($aplicacoes->{$this->aplicacao})) {
                $proxyusername = USUARIO_INTEGRACAO_PADRAO;
                $proxypassword = SENHA_INTEGRACAO_PADRAO;
            } else {
                $proxyusername = $aplicacoes->{$this->aplicacao}->usuario;
                $proxypassword = $aplicacoes->{$this->aplicacao}->senha;
            }
        } else {
            $proxyusername = $usuario;
            $proxypassword = $senha;
        }
        $useCURL = '0';

        switch ($ws) {
            case 1:
                $ws = $this->ws;
            break;
            case 2:
                $ws = $this->wsConsulta;
            break;
            case 3:
                $ws = $this->wsEdu;
            break;
            case 4:
                $ws = $this->wsReport;
            break;
        }

        $this->clienteWS = new \nusoap_client(
            ENDERECO_BASE_INTEGRACAO.'/'.$ws,
            'wsdl',
            '',
            '',
            $proxyusername,
            $proxypassword
        );

        $this->clienteWS->timeout = 1000;
        $this->clienteWS->response_timeout = 1000;

        $err = $this->clienteWS->getError();


        if ($err) {
            echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }


        $this->clienteWS->setUseCurl($useCURL);
        $this->clienteWS->loadWSDL();
        $this->clienteWS->setCredentials($proxyusername, $proxypassword);
        $this->clienteWS->soap_defencoding = 'UTF-8';
    }

    public function autenticaAcesso($usuario, $senha)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(3, $usuario, $senha);
        $this->clienteWS->call('AutenticaAcesso');

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada('', 'AutenticaAcesso', 'usuario='.$usuario.';senha='.$senha, '', '', 'AutenticaAcesso', $usuario);

        return $this->clienteWS->responseData;
    }

    public function realizarConsultaSQL($codigoConsulta, $coligada, $codSistema, $paramentro)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(2);

        if (trim($paramentro) == '' || is_null($paramentro)) {
            $result = $this->clienteWS->call('RealizarConsultaSQL', array(array('codSentenca'=>$codigoConsulta,
                                                                        'codColigada'=> $coligada,
                                                                        'codSistema'=>$codSistema)));
        } else {
            $result = $this->clienteWS->call('RealizarConsultaSQL', array(array('codSentenca'=>$codigoConsulta,
                                                                                'codColigada'=>$coligada,
                                                                                'codSistema'=>$codSistema,
                                                                                'parameters'=>$paramentro
                                                                                )));
        }
        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($codigoConsulta, 'RealizarConsultaSQL', $paramentro, 'codColigada='.$coligada.';codSistema='.$codSistema, '', 'RealizarConsultaSQL', $codigoConsulta);

        return $this->clienteWS->responseData;
    }

    public function getReadView($dataServer, $filtro, $contexto)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar();

        $this->clienteWS->call('ReadView', array('DataServerName'=>$dataServer,
                                                    'Filtro'=> $filtro,
                                                    'Contexto'=>$contexto));

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($dataServer, 'ReadView', '', $contexto, $filtro, 'ReadView', $dataServer);

        return $this->clienteWS->responseData;
    }

    /**
     * Método utilizado para gerar o XML para o saveRecord
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param String $chave Chaves primarias separadas por ; (somente valores)
     * @param String $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function readRecord($dataServer, $chave, $contexto)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar();
        $this->clienteWS->call('ReadRecord', array('DataServerName'=>$dataServer,
                                                    'PrimaryKey'=>$chave,
                                                    'Contexto'=>$contexto));
        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada($dataServer, 'ReadRecord', '', $contexto, $chave, 'ReadRecord', $dataServer);

        return $this->clienteWS->responseData;
    }

    /**
     * Método utilizado para inserir registro
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param type $xml XML gerado no "readRecord"
     * @param type $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function saveRecord($dataServer, $xml, $contexto)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar();

        $this->clienteWS->call('SaveRecord', array('DataServerName'=>$dataServer,
                                                    'XML'=>str_replace('<?xml version="1.0"?>', '', $xml),
                                                    'Contexto'=>$contexto));

        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada($dataServer, 'SaveRecord', '', $contexto, '', 'ReadRecord', $dataServer, $xml);

        return $this->clienteWS->responseData;
    }

    private function gerarFiltro($configuracao, $indice, $filtro)
    {
        $filtroString = $configuracao['filtro_'.$indice];
        for ($i=0;$i<count($filtro);$i++) {
            $filtroString = str_replace('[['.($i+1).']]', $filtro[$i], $filtroString);
        }
        return $filtroString;
    }

    private function gerarParametros($configuracao, $indice)
    {
        return $configuracao['parametro_'.$indice];
    }

    public function genereteReport($coligada, $codRelatorio, $indice, $filtro)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $configuracao = json_decode(file_get_contents(DIR_BASE.'/'.DEFINICAO_FILTRO_INTEGRADA));

        $this->conectar(4);
        $filtroXML = $this->gerarFiltro($configuracao, $indice, $filtro);
        $parametro = $this->gerarParametros($configuracao, $indice);

        $this->clienteWS->call(
            'GenereteReportInfo',
            array('codColigada'=> $coligada,
                'id'=> $codRelatorio,
                'filters'=>  $filtroXML,
                'parameters'=>  $parametro,
                'fileName'=>  'Boleto.pdf'
            )
        );

        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada('', 'GenereteReportInfo', '', '', '', 'GenereteReportInfo', $codRelatorio, $filtroXML);

        $xml = simplexml_load_string(str_replace(
            ['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', 's:Body', 's:Envelope'],
            ['','', '', '', 'sBody', 'sEnvelope'],
            $this->clienteWS->responseData
        ));

        if ($xml) {
            $this->inicio = date('Y-m-d H:i:s');

            $this->clienteWS->call('GetGeneratedReportSize', array(array(
                'guid'=> $xml->sBody->GenerateReportResponse->GenerateReportResult
            )));

            $this->fim = date('Y-m-d H:i:s');
            $this->registrarChamada('', 'GetGeneratedReportSize', '', '', 'guid='.$xml->sBody->GenerateReportResponse->GenerateReportResult, '', '', 'GetGeneratedReportSize', $codRelatorio);

            $xmlSize = simplexml_load_string(str_replace(
                ['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', 's:Body', 's:Envelope'],
                ['','', '', '', 'sBody', 'sEnvelope'],
                $this->clienteWS->responseData
            ));

            if ($xmlSize) {
                $this->inicio = date('Y-m-d H:i:s');

                $this->clienteWS->call(
                    'GetFileChunk',
                    array(
                        array(
                            'guid'=> $xml->sBody->GenerateReportResponse->GenerateReportResult,
                            'offset'=> 0,
                            'length' => $xmlSize->sBody->GetGeneratedReportSizeResponse->GetGeneratedReportSizeResult
                        )
                    )
                );

                $this->fim = date('Y-m-d H:i:s');
                $this->registrarChamada('', 'GetFileChunk', 'guid='.$xml->sBody->GenerateReportResponse->GenerateReportResult.';length='.$xmlSize->sBody->GetGeneratedReportSizeResponse->GetGeneratedReportSizeResult, '', '', 'GetFileChunk', $codRelatorio);
            }
        }
        return $this->clienteWS->responseData;
    }

    public function arquivoCache($idReal)
    {
        $this->registrar->setId($idReal);
        return file_get_contents($this->registrar->carregar('resposta', false, '{resposta}'));
    }

    private function registrarChamada(
        $dataServer,
        $metodo,
        $parametro,
        $contexto,
        $filtro,
        $acao,
        $segundoNivel,
        $campo=false
    ) {
        mkdir(DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/');
        mkdir(DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/');

        $enderecoDebug = DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/debug_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
        $enderecoRequest = DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/request_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
        $enderecoResponse = DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/response_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';

        Arquivo::criaArquivo($this->clienteWS->debug_str, $enderecoDebug);
        Arquivo::criaArquivo($this->clienteWS->request, $enderecoRequest);
        Arquivo::criaArquivo($this->clienteWS->responseData, $enderecoResponse);


        if ($campo) {
            $enderecoCampo = DIR_BASE.'/file/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/campo_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
            Arquivo::criaArquivo($this->clienteWS->responseData, $enderecoCampo);
        }

        if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        }

        $msg = Conteiner::get('Mensagem');

        $this->registrar->setInicio($this->inicio);
        $this->registrar->setFim($this->fim);
        $this->registrar->setProcessoTotvsIdentificador($msg->getCampo('ProcessoTotvs::id')->get('valor'));

        $this->registrar->setRegistrarChamadaWebServiceTotvs($msg->getCampo('RegistrarChamadaWebServiceTotvs::id')->get('valor'));

        $this->registrar->setMetodo($metodo);
        $this->registrar->setAgendaProcesso($msg->getCampo('AgendaProcesso::id')->get('valor'));
        $this->registrar->setParametro($parametro);
        $this->registrar->setDataServer($dataServer);
        $this->registrar->setContexto($contexto);
        $this->registrar->setResposta($enderecoResponse);
        $this->registrar->setEnvio($enderecoRequest);
        $this->registrar->setDebug($enderecoDebug);
        $this->registrar->setCampo($enderecoCampo);
        $this->registrar->setFiltro($filtro);
        $this->registrar->setMomento(date('Y-m-d H:i:s'));
        $this->registrar->setAtivo(1);
        $this->registrar->salvar();
        $this->registrar->limparObjeto();
        Persistencia::commit();
        
        if (Persistencia::getBaseAtual() != "principal") {
            Persistencia::mudarBase("principal");
        }
    }
}
