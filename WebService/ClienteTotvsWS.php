<?php
namespace Rubeus\IntegracaoTotvs\WebService;
use Rubeus\ContenerDependencia\Conteiner;

class ClienteTotvsWS {
    private $coneccao;
    private $url;

    private function conectar(){
        $this->coneccao = curl_init();
    }

    private function prepararCampoEnvio($campos, $url){
        foreach($campos as $key=>$value) { $campos_string .= $key.'='.$value.'&'; }
            rtrim($campos_string, '&');

        $this->url = $url;
        curl_setopt($this->coneccao,CURLOPT_URL, $url);
        curl_setopt($this->coneccao,CURLOPT_POST, count($campos));
        curl_setopt($this->coneccao,CURLOPT_POSTFIELDS, $campos_string);
        curl_setopt($this->coneccao,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->coneccao,CURLOPT_VERBOSE, 1);
    }

    private function chamar(){
        //$tentou = 0;
        ob_start();
        //do{
          $resultado = curl_exec($this->coneccao);
          $debug = ob_get_contents();
          //$tentou++;
        //}while(trim($resultado) !== '' && $tentou < 3 );
        ob_end_clean();

        if(curl_error($this->coneccao)) {
          $msg = Conteiner::get('Mensagem');
          $msg->setCampo('ERROR_ERROR', curl_error($this->coneccao));
          $msg->setCampo('ERROR_URL', $this->url);
        }
        curl_close($this->coneccao);

        return $resultado;
    }

    public function getReadView($dataServer, $filtro, $contexto){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=getReadView';

        $campos = array(
            'dataServer' => urlencode($dataServer),
            'filtro' => urlencode($filtro),
            'contexto' => urlencode($contexto),
            'aplicacao' => APLICACAO
        );

        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

    public function autenticaAcesso($usuario, $senha){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=autenticarAcesso';

        $campos = array(
            'usuario' => urlencode($usuario),
            'senha' => urlencode($senha),
            'aplicacao' => APLICACAO
        );

        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

    public function boletoBase64($codColigada, $codRelatorio, $codBoleto){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=gerarBoleto&coligada='.urlencode($codColigada).
                '&codRelatorio='.urlencode($codRelatorio).'&idBoleto='.urlencode($codBoleto).'&aplicacao='.APLICACAO;

        $this->conectar();
        $this->prepararCampoEnvio([], $url);
        return $this->chamar();
    }

    /**
     * Método utilizado para gerar o XML para o saveRecord
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param String $chave Chaves primarias separadas por ; (somente valores)
     * @param String $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function readRecord($dataServer, $chave, $contexto){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=readRecord';

        $campos = array(
            'dataServer' => urlencode($dataServer),
            'chave' => urlencode($chave),
            'contexto' => urlencode($contexto),
            'aplicacao' => APLICACAO
        );

        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

    /**
     * Método utilizado para inserir registro
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param type $xml XML gerado no "readRecord"
     * @param type $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function saveRecord($dataServer, $xml, $contexto){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=saveRecord';
        $campos = array(
            'dataServer' => urlencode($dataServer),
            'xml' => urlencode($xml),
            'contexto' => urlencode($contexto),
            'aplicacao' => APLICACAO
        );
        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

    public function report($codColigada,$codRelatorio,$filtro,$parametro){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=report';
        $campos = array(
            'codColigada' => urlencode($codColigada),
            'codRelatorio' => urlencode($codRelatorio),
            'filtro' => urlencode($filtro),
            'paramentro' => urlencode($parametro),
            'aplicacao' => APLICACAO
        );
        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }


    public function realizarConsultaSQL($codSentenca, $codColigada, $codSistema,$parametro){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=realizarConsultaSQL';
        $campos = array(
            'codSentenca' => urlencode($codSentenca),
            'codColigada' => urlencode($codColigada),
            'codSistema' => urlencode($codSistema),
            'parametro' => urlencode($parametro),
            'aplicacao' => APLICACAO
        );

        $this->conectar();
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

    public function disponibilidade(){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=disponibilidade';
        $this->conectar();
        curl_setopt($this->coneccao,CURLOPT_URL, $url);
        return $this->chamar();
    }

    public function arquivoCache($idReal){
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=arquivoCache';
        $this->conectar();
        $campos = array(
            'idReal' => urlencode($idReal)
        );
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }

}
