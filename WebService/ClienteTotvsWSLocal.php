<?php
namespace Rubeus\IntegracaoTotvs\WebService;

use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\IntegracaoTotvs\Registrar\RegistrarChamadaTotvs;
use Rubeus\Bd\Persistencia;
use Rubeus\Arquivos\Arquivo;

require_once(\Ambiente::dir(__DIR__).'/../lib/nusoap.php');
require_once(\Ambiente::dir(__DIR__).'/../lib/class.wsdlcache.php');

class ClienteTotvsWSLocal
{
    private $clienteWS = false;
    private $ws;
    private $wsConsulta;
    private $wsEdu;
    private $wsReport;
    private $wsFin;
    private $wsProcesso;
    private $aplicacao;
    private $inicio;
    private $fim;
    private $registrar;
    private $usuario;
    private $senha;
    private $dirBase;
    private $debugAtivo;

    public function __construct()
    {
        $this->ws = (defined('TBC_SEM_LINCENCA') && TBC_SEM_LINCENCA==1) ? 'EduLicenseWSDataServer/MEX?wsdl' : (TBC_HOST == 1 ? 'wsDataServer/MEX?wsdl' : 'wsDataServer.asmx?wsdl');
        $this->wsDataServerAntigo = TBC_HOST == 1 ? 'wsDataServer/MEX?wsdl' : 'wsDataServer.asmx?wsdl';
        $this->wsConsulta = (defined('TBC_SEM_LINCENCA') && TBC_SEM_LINCENCA==1) ? 'EduLicenseWSConsultaSQL/MEX?wsdl' : (TBC_HOST == 1 ? 'wsConsultaSQL/MEX?wsdl' : 'wsConsultaSQL.asmx?wsdl');
        $this->wsEdu = 'wsEdu/MEX?wsdl';
        $this->wsReport = (defined('TBC_SEM_LINCENCA') && TBC_SEM_LINCENCA==1) ? 'EduLicenseWSReport/MEX?wsdl' : 'wsReport/MEX?wsdl';
        $this->wsFin = 'wsFin/MEX?wsdl';
        $this->wsCrm = 'wsCRMAtendimento/MEX?wsdl';
        $this->wsProcesso = (defined('TBC_SEM_LINCENCA') && TBC_SEM_LINCENCA==1) ? 'EduLicenseWSProcess/MEX?wsdl' : (TBC_HOST == 1 ? 'wsProcess/MEX?wsdl' : 'wsProcess.asmx?wsdl');
        $this->wsProcessAntigo = TBC_HOST == 1 ? 'wsProcess/MEX?wsdl' : 'wsProcess.asmx?wsdl';
        $this->aplicacao = false;
        $this->registrar = new RegistrarChamadaTotvs();
        if (defined('DIR_FILE_INSTITUICAO')) {
            $this->dirBase = DIR_BASE.'/file/'.DIR_FILE_INSTITUICAO.'/';
        } else {
            $this->dirBase = DIR_BASE.'/file/';
        }
        if (defined("DEBUG_ATIVO")) {
            $this->debugAtivo=DEBUG_ATIVO;
        } else {
            $this->debugAtivo=0;
        }
    }

    private function conectar($ws=1, $usuario=false, $senha=false)
    {
        if ($this->clienteWS && $this->wsAnterior == $ws && !$usuario) {
            return;
        }
        if (!$usuario) {
            $aplicacoes = json_decode(file_get_contents(DIR_BASE.'/'.DEFINICAO_APLICACAO_INTEGRADA));
            if (!isset($aplicacoes->{$this->aplicacao})) {
                $proxyusername = USUARIO_INTEGRACAO_PADRAO;
                $proxypassword = SENHA_INTEGRACAO_PADRAO;
            } else {
                $proxyusername = $aplicacoes->{$this->aplicacao}->usuario;
                $proxypassword = $aplicacoes->{$this->aplicacao}->senha;
            }
        } else {
            $proxyusername = $usuario;
            $proxypassword = $senha;
        }
        $useCURL = '0';

        switch ($ws) {
            case 1:
                $ws = $this->ws;
            break;
            case 2:
                $ws = $this->wsConsulta;
            break;
            case 3:
                $ws = $this->wsEdu;
            break;
            case 4:
                $ws = $this->wsReport;
            break;
            case 5:
                $ws = $this->wsProcesso;
            break;
            case 6:
                $ws = $this->wsFin;
            break;
            case 7:
                $ws = $this->wsCrm;
            break;
            case 8:
                $ws = $this->wsDataServerAntigo;
            break;
            case 9:
                $ws = $this->wsProcessAntigo;
            break;
        }

        $url = ENDERECO_BASE_INTEGRACAO.'/'.$ws;

        $this->senha = $proxypassword;
        $this->usuario = $proxyusername;

        $cache = new \nusoap_wsdlcache('/tmp', 86400);
        $wsdl = $cache->get($url);
        if (is_null($wsdl)) {
            $wsdl = new \wsdl($url, '', '', '', '', 5);
            $cache->put($wsdl);
        }
        $this->clienteWS = new \nusoap_client($wsdl, 'wsdl', '', '', '', '');

        $this->clienteWS->timeout = 1000;
        $this->clienteWS->response_timeout = 1000;

        $err = $this->clienteWS->getError();


        if ($err) {
            //echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
        }


        $this->clienteWS->setUseCurl($useCURL);
        $this->clienteWS->loadWSDL();
        $this->clienteWS->setCredentials($proxyusername, $proxypassword);
        $this->clienteWS->soap_defencoding = 'UTF-8';
    }

    public function getReadView($dataServer, $filtro, $contexto)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar();

        if (TBC_HOST == 1) {
            $this->clienteWS->call('ReadView', array('DataServerName'=>$dataServer,
                                                        'Filtro'=> $filtro,
                                                        'Contexto'=>$contexto));
        } else {
            $this->clienteWS->call('ReadViewAuth', array('DataServerName'=>$dataServer,
                                                        'Filtro'=> $filtro,
                                                        'Usuario'=> $this->usuario,
                                                        'Senha'=> $this->senha,
                                                        'Contexto'=>$contexto));
        }



        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($dataServer, 'ReadView', '', $contexto, $filtro, 'ReadView', $dataServer);

        return $this->clienteWS->responseData;
    }

    public function autenticaAcesso($usuario, $senha)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(3, $usuario, $senha);
        if (TBC_HOST == 1) {
            $this->clienteWS->call('AutenticaAcesso');
        } else {
            $this->clienteWS->call('AutenticaAcessoAuth');
        }

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada('', 'AutenticaAcesso', 'usuario='.$usuario.';senha='.$senha, '', '', 'AutenticaAcesso', $usuario);

        return $this->clienteWS->responseData;
    }

    public function imprimirBoletoEducacional($idBoleto, $codColigada, $CodFilial, $tipoCurso)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(3);

        $parametro = [
            'IdBoleto' => $idBoleto,
            'CodColigada' => $codColigada,
            'CodFilial' => $CodFilial,
            'CodTipoCurso' => $tipoCurso,
            'Usuario' => $this->usuario
        ];
        $this->clienteWS->call('ImprimeBoletoParam', $parametro);

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada('', 'ImprimeBoletoParam', implode(';', $parametro), '', '', 'ImprimeBoletoParam', $usuario);

        return $this->clienteWS->responseData;
    }

    /**
     * Método utilizado para gerar o XML para o saveRecord
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param String $chave Chaves primarias separadas por ; (somente valores)
     * @param String $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function readRecord($dataServer, $chave, $contexto, $usuario=false, $senha=false, $utilizarMetodoAntigo = false)
    {
        $this->inicio = date('Y-m-d H:i:s');

        if ($utilizarMetodoAntigo) {
            $this->conectar(8);
        } else {
            $this->conectar();
        }
        if (TBC_HOST == 1) {
            $this->clienteWS->call('ReadRecord', array('DataServerName'=>$dataServer,
                                                        'PrimaryKey'=>$chave,
                                                        'Contexto'=>$contexto));
        } else {
            $this->clienteWS->call('ReadRecordAuth', array('DataServerName'=>$dataServer,
                                                        'PrimaryKey'=>$chave,
                                                        'Usuario'=> $this->usuario,
                                                        'Senha'=> $this->senha,
                                                        'Contexto'=>$contexto));
        }
        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada($dataServer, 'ReadRecord', '', $contexto, $chave, 'ReadRecord', $dataServer);

        return $this->clienteWS->responseData;
    }

    /**
     * Método utilizado para inserir registro
     *
     * @param String $dataServer Nome do objeto de negócio
     * @param type $xml XML gerado no "readRecord"
     * @param type $contexto Contexto da busca (CODSISTEMA=G,CODUSUARIO=mestre,CODCOLIGADA=1)
     */
    public function saveRecord($dataServer, $xml, $contexto, $usuario=false, $senha=false, $utilizarMetodoAntigo = false)
    {
        $this->inicio = date('Y-m-d H:i:s');

        if ($utilizarMetodoAntigo) {
            $this->conectar(8, $usuario, $senha);
        } else {
            $this->conectar(1, $usuario, $senha);
        }
        if (TBC_HOST == 1) {
            $this->clienteWS->call('SaveRecord', array('DataServerName'=>$dataServer,
                                                        'XML'=>str_replace('<?xml version="1.0"?>', '', $xml),
                                                        'Contexto'=>$contexto));
        } else {
            $this->clienteWS->call('SaveRecordAuth', array('DataServerName'=>$dataServer,
                                                        'Usuario'=> $this->usuario,
                                                        'Senha'=> $this->senha,
                                                        'XML'=>str_replace('<?xml version="1.0"?>', '', $xml),
                                                        'Contexto'=>$contexto));
        }

        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada($dataServer, 'SaveRecord', '', $contexto, '', 'ReadRecord', $dataServer, $xml);

        return $this->clienteWS->responseData;
    }

    public function criarAtendimento($paramentros, $usuario, $senha)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(7, $usuario, $senha);

        $this->clienteWS->call('criarAtendimento', $paramentros);

        $this->fim = date('Y-m-d H:i:s');
        $this->registrarChamada($dataServer, 'criarAtendimento', implode(',', $paramentros), '', '', 'criarAtendimento', $dataServer, $xml);

        return $this->clienteWS->responseData;
    }


    public function realizarConsultaSQL($codigoConsulta, $coligada, $codSistema, $paramentro)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(2);

        if (TBC_HOST == 1) {
            if (trim($paramentro) == '' || is_null($paramentro)) {
                $result = $this->clienteWS->call('RealizarConsultaSQL', array(array('codSentenca'=>$codigoConsulta,
                                                                            'codColigada'=> $coligada,
                                                                            'codSistema'=>$codSistema)));
            } else {
                $result = $this->clienteWS->call('RealizarConsultaSQL', array(array('codSentenca'=>$codigoConsulta,
                                                                                    'codColigada'=>$coligada,
                                                                                    'codSistema'=>$codSistema,
                                                                                    'parameters'=>$paramentro
                                                                                    )));
            }
        } else {
            if (trim($paramentro) == '' || is_null($paramentro)) {
                $result = $this->clienteWS->call('RealizarConsultaSQLAuth', array(array('codSentenca'=>$codigoConsulta,
                                                                            'Usuario'=> $this->usuario,
                                                                            'Senha'=> $this->senha,
                                                                            'codColigada'=> $coligada,
                                                                            'codAplicacao'=>$codSistema)));
            } else {
                $result = $this->clienteWS->call('RealizarConsultaSQLAuth', array(array('codSentenca'=>$codigoConsulta,
                                                                                    'Usuario'=> $this->usuario,
                                                                                    'Senha'=> $this->senha,
                                                                                    'codColigada'=>$coligada,
                                                                                    'codAplicacao'=>$codSistema,
                                                                                    'parameters'=>$paramentro
                                                                                    )));
            }
        }

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($codigoConsulta, 'RealizarConsultaSQL', $paramentro, 'codColigada='.$coligada.';codSistema='.$codSistema, '', 'RealizarConsultaSQL', $codigoConsulta);

        return $this->clienteWS->responseData;
    }

    public function executarProcessoParametros($codigoProcesso, $xml, $utilizarMetodoAntigo = false)
    {
        $this->inicio = date('Y-m-d H:i:s');

        
        if ($utilizarMetodoAntigo) {
            $this->conectar(9, $usuario, $senha);
        } else {
            $this->conectar(5, $usuario, $senha);
        }

        $result = $this->clienteWS->call('ExecuteWithParams', array('ProcessServerName'=>$codigoProcesso,
                                                                            'strXmlParams'=> $xml));

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($codigoProcesso, 'ExecuteWithParams', $xml, '', '', 'ExecuteWithParams', $codigoProcesso);

        return $this->clienteWS->responseData;
    }

    public function executarProcesso($codigoProcesso, $xml, $usuario = false, $senha = false, $utilizarMetodoAntigo = false)
    {
        $this->inicio = date('Y-m-d H:i:s');

        if ($utilizarMetodoAntigo) {
            $this->conectar(9, $usuario, $senha);
        } else {
            $this->conectar(5, $usuario, $senha);
        }

        $result = $this->clienteWS->call('ExecuteWithXmlParams', array('ProcessServerName'=>$codigoProcesso,
                                                                            'strXmlParams'=> $xml));

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($codigoProcesso, 'ExecuteWithXmlParams', $xml, '', '', 'ExecuteWithXmlParams', $codigoProcesso);

        return $this->clienteWS->responseData;
    }

    public function executarProcessoFinanceiro($contexto, $xml, $processo)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(6);

        $result = $this->clienteWS->call($processo, array('xml'=>$xml,
                                                        'contexto'=> $contexto));

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($processo, $processo, $xml, $contexto, '', $processo, $processo);

        return $this->clienteWS->responseData;
    }

    public function report($codColigada, $codRelatorio, $filtro, $parametro)
    {
        $this->inicio = date('Y-m-d H:i:s');

        $this->conectar(4);

        $result = $this->clienteWS->call('GenerateReport', array(array('codColigada'=>$codColigada,
                                                                'id'=> $codRelatorio,
                                                                'filters'=> $filtro,
                                                                'parameters'=> $parametro,
                                                                'fileName'=> 'relatorio.pdf')));

        $this->fim = date('Y-m-d H:i:s');

        $this->registrarChamada($codRelatorio, 'GenerateReport', $parametro, '', $filtro, 'GenerateReport', $codRelatorio);

        $xml = simplexml_load_string(str_replace(
            ['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', 's:Body', 's:Envelope'],
            ['','', '', '', 'sBody', 'sEnvelope'],
            $this->clienteWS->responseData
        ));

        if ($xml) {
            $this->clienteWS->call('GetGeneratedReportSize', array(array(
                'guid'=> rtrim($xml->sBody->GenerateReportResponse->GenerateReportResult)
            )));
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_start();
            }
            $_SESSION['ultimoGuid'] = rtrim($xml->sBody->GenerateReportResponse->GenerateReportResult);
            if (defined('NOLOCK_SESSION') && NOLOCK_SESSION == 1) {
                session_write_close();
            }

            $this->registrarChamada($codRelatorio, 'GetGeneratedReportSize', rtrim($xml->sBody->GenerateReportResponse->GenerateReportResult), '', '', 'GetGeneratedReportSize', $codRelatorio);

            $xmlSize = simplexml_load_string(str_replace(
                ['&#x1F;','&#x1f;', '&amp;#x1F;', '&amp;#x1f;', 's:Body', 's:Envelope'],
                ['','', '', '', 'sBody', 'sEnvelope'],
                $this->clienteWS->responseData
            ));

            if ($xmlSize) {
                $this->clienteWS->call('GetFileChunk', array(array(
                        'guid'=> rtrim($xml->sBody->GenerateReportResponse->GenerateReportResult),
                        'offset'=> 0,
                        'length' => rtrim($xmlSize->sBody->GetGeneratedReportSizeResponse->GetGeneratedReportSizeResult)
                    )));

                $this->registrarChamada($codRelatorio, 'GetFileChunk', rtrim($xml->sBody->GenerateReportResponse->GenerateReportResult), '', rtrim($xmlSize->sBody->GetGeneratedReportSizeResponse->GetGeneratedReportSizeResult), 'GetFileChunk', $codRelatorio);
            }
        }
        return $this->clienteWS->responseData;
    }

    public function disponibilidade()
    {
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=disponibilidade';
        $this->conectar();
        curl_setopt($this->coneccao, CURLOPT_URL, $url);
        return $this->chamar();
    }

    public function arquivoCache($idReal)
    {
        $url = Conteiner::get('WebServiceLocalTovs').'?metodo=arquivoCache';
        $this->conectar();
        $campos = array(
            'idReal' => urlencode($idReal)
        );
        $this->prepararCampoEnvio($campos, $url);
        return $this->chamar();
    }


    private function registrarChamada($dataServer, $metodo, $parametro, $contexto, $filtro, $acao, $segundoNivel, $campo=false)
    {
        mkdir($this->dirBase.'/logchamadawstotvs/'.$acao.'/');
        mkdir($this->dirBase.'/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/');
        if (trim($this->clienteWS->responseData) == '') {
            $this->debugAtivo=1;
        }
        if ($this->debugAtivo==1) {
            $enderecoDebug = $this->dirBase.'/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/debug_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
        }
        $enderecoRequest = $this->dirBase.'/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/request_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
        $enderecoResponse = $this->dirBase.'/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/response_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';

        if ($this->debugAtivo==1) {
            Arquivo::criaArquivo($this->clienteWS->debug_str, $enderecoDebug);
        }
        Arquivo::criaArquivo($this->clienteWS->request, $enderecoRequest);
        Arquivo::criaArquivo($this->clienteWS->responseData, $enderecoResponse);

        if ($campo) {
            $enderecoCampo = $this->dirBase.'/logchamadawstotvs/'.$acao.'/'.$segundoNivel.'/campo_'.date('Y_m_d_H_i_s_').rand(100, 1300).'.xml';
            Arquivo::criaArquivo($campo, $enderecoCampo);
        }

        if (Persistencia::getBaseAtual() != Conteiner::get("baseRegistrarIntegracao")) {
            Persistencia::mudarBase(Conteiner::get("baseRegistrarIntegracao"));
        }
        $msg = Conteiner::get('Mensagem');

        $this->registrar->setInicio($this->inicio);
        $this->registrar->setFim($this->fim);
        $this->registrar->setProcessoTotvsIdentificador($msg->getCampo('ProcessoTotvs::id')->get('valor'));

        $this->registrar->setRegistrarChamadaWebServiceTotvs($msg->getCampo('RegistrarChamadaWebServiceTotvs::id')->get('valor'));

        $this->registrar->setMetodo($metodo);
        $this->registrar->setAgendaProcesso($msg->getCampo('AgendaProcesso::id')->get('valor'));
        $this->registrar->setParametro($parametro);
        $this->registrar->setDataServer($dataServer);
        $this->registrar->setContexto($contexto);
        $this->registrar->setResposta($enderecoResponse);
        $this->registrar->setEnvio($enderecoRequest);
        if ($this->debugAtivo==1) {
            $this->registrar->setDebug($enderecoDebug);
        }
        $this->registrar->setCampo($enderecoCampo);
        $this->registrar->setFiltro($filtro);
        $this->registrar->setMomento(date('Y-m-d H:i:s'));
        $this->registrar->setAtivo(1);
        $this->registrar->salvar();

        Conteiner::registrar('ultimoRegistroChamadaTotvs', $this->registrar->getId());
        if (trim($this->clienteWS->responseData) == '' || strpos($this->clienteWS->responseData, 'DOCTYPE HTML')) {
            Conteiner::get('ErroProcessoIntegracao')->registrar($this->registrar->getId(), '');
        }
        $this->registrar->limparObjeto();
        Persistencia::commit();

        if (Persistencia::getBaseAtual() != "principal") {
            Persistencia::mudarBase("principal");
        }
        
        if (defined("DEBUG_ATIVO")) {
            $this->debugAtivo=DEBUG_ATIVO;
        } else {
            $this->debugAtivo=0;
        }
    }
}
