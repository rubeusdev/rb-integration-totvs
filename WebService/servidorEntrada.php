<?php
ini_set('display_errors',0);
error_reporting(-1);

use Rubeus\Processo\Aplicacao\Entrada;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Bd\Persistencia;
use Rubeus\Processo\Dominio\Processo\RepositorioProcesso;

function entradaTotvs($processo,$etapa,$usuario,$formulaVisual,$parametro1, $parametro2, $parametro3, $parametro4, $parametro5, $parametro6,
                        $parametro7, $parametro8, $parametro9, $parametro10){
    
    $msg = Conteiner::get('Mensagem');
    $msg->setCampo('Processo',$processo);
    $msg->setCampo('Usuario',$usuario);
    $msg->setCampo('Etapa',$etapa);
    $msg->setCampo('FormulaVisual',$formulaVisual);
    //$msg->setCampo('Momento',$momento);
    $msg->setCampo('Parametro1',$parametro1);
    $msg->setCampo('Parametro2',$parametro2);
    $msg->setCampo('Parametro3',$parametro3);
    $msg->setCampo('Parametro4',$parametro4);
    $msg->setCampo('Parametro5',$parametro5);
    $msg->setCampo('Parametro6',$parametro6);
    $msg->setCampo('Parametro7',$parametro7);
    $msg->setCampo('Parametro8',$parametro8);
    $msg->setCampo('Parametro9',$parametro9);
    $msg->setCampo('Parametro10',$parametro10);
    RepositorioProcesso::get($processo, $etapa)->executar($msg, true);
    return true;
}

ini_set("soap.wsdl_cache_enabled", "0");
ini_set ('default_socket_timeout' ,  60000);

$endereco = DOMINIO_PROJETO."/wsdl?root=integracaoTotvs";

$server = new SoapServer($endereco, array('encoding'=>'ISO-8859-1'));
$server->addFunction("entradaTotvs");
$server->handle();